from PyQt5.uic import loadUi
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import os
class BitDialog(QDialog):
    def __init__(self,info, bitNames,preset=0):
        super().__init__()
        # Load the dialog's GUI
        DIRNAME = os.path.split(__file__)[0]
        ui_file = os.path.join(DIRNAME,"..", "ADTMasterApp","ui/bitdialog.ui")
        loadUi(ui_file, self)
        for element in bitNames.keys():
            if type(element)==int and element>=0 and element<=15:
                widget = self.findChild(QRadioButton, "bit"+str(element))
                widget.setText(bitNames[element])
        print(preset)
        for i in range(16):
            if preset and (preset & (1<<i)):
                widget = self.findChild(QRadioButton, "bit"+str(i))
                widget.setChecked(True)
        self.label.setText(info)
        self.OK.clicked.connect(self.close)
        self.CANCEL.clicked.connect(self.cancel)
    def close(self):
        self.done(1)
    def cancel(self):
        self.done(0)
    def get(self):
        self.show()
        if self.exec_():
            val=0
            if self.bit0.isChecked():
                val+=2**0
            if self.bit1.isChecked():
                val+=2**1
            if self.bit2.isChecked():
                val+=2**2
            if self.bit3.isChecked():
                val+=2**3
            if self.bit4.isChecked():
                val+=2**4
            if self.bit5.isChecked():
                val+=2**5
            if self.bit6.isChecked():
                val+=2**6
            if self.bit7.isChecked():
                val+=2**7
            if self.bit8.isChecked():
                val+=2**8
            if self.bit9.isChecked():
                val+=2**9
            if self.bit10.isChecked():
                val+=2**10
            if self.bit11.isChecked():
                val+=2**11
            if self.bit12.isChecked():
                val+=2**12
            if self.bit13.isChecked():
                val+=2**13
            if self.bit14.isChecked():
                val+=2**14
            if self.bit15.isChecked():
                val+=2**15
            return val


        else:
            return None
        #self.show()

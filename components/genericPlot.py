from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import numpy as np
from PyQt5 import QtCore, QtWidgets,QtGui
import logging

#FigureCanvas
class GenericPlot(QtWidgets.QWidget):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.canvas = FigureCanvas(self.fig)
        self.axes = self.fig.add_subplot(111)
        super(GenericPlot, self).__init__()
        self.x_data=np.linspace(0,9999,10000)
        self.y_data=np.zeros(10000)
        self.axes.plot(self.x_data, self.y_data)
        self.axes.set_xlim([0,10000])
        self.axes.set_ylim([0,2**15])
        self.layout=QtWidgets.QVBoxLayout()
        self.windowProfileToolbar = NavigationToolbar(self.canvas, self)
        self.layout.addWidget(self.windowProfileToolbar)
        self.layout.addWidget(self.canvas)
        self.setLayout(self.layout)
        self.title_name="a plot"
        self.axes.set_title(self.title_name)
        self.axes.grid(True)
        
    def setTitle(self,name):
        self.axes.set_title(name)
        self.title_name=name
    def plot(self,x_data,y_data,line="-",label=""):
        self.x_data.append(x_data)
        self.y_data.append(y_data)
        self.axes.plot(x_data, y_data,line,label=label)
        self.axes.set_xlim([np.amin(self.x_data),np.amax(self.x_data)])
        self.axes.set_ylim([np.amin(self.y_data),np.amax(self.y_data)])
        if label!="":
             self.axes.legend(loc='lower left')
    def draw(self):
        self.canvas.draw()
    def clear(self):
        self.axes.cla()
        self.x_data=[]
        self.y_data=[]
        self.axes.set_title(self.title_name)
        self.axes.grid(True)

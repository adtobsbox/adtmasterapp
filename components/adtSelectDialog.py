from PyQt5.uic import loadUi
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import os


class adtSelectDialog(QDialog):
	def __init__(self, info, names):
		super(adtSelectDialog, self).__init__()
		vlayout = QVBoxLayout(self)
		selectLayout = QHBoxLayout()
		okCancelLayout = QHBoxLayout()
		vlayout.addLayout(selectLayout)
		vlayout.addLayout(okCancelLayout)
		b1 = QPushButton("OK")
		b1.clicked.connect(self.close)
		okCancelLayout.addWidget(b1)
		b1 = QPushButton("CANCEL")
		b1.clicked.connect(self.cancel)
		okCancelLayout.addWidget(b1)
		self.names=names
		
		for name in names:
			b1 = QRadioButton(name)
			b1.setChecked(False)
			b1.setObjectName(name)
			selectLayout.addWidget(b1)
		self.setWindowTitle(info)
	def close(self):
		self.done(1)
	def cancel(self):
		self.done(0)
	def get(self):
		self.show()
		if self.exec_():
			to_return={}
			for button in self.names:
				widget = self.findChild(QRadioButton, button)
				if widget:
					to_return[button]=widget.isChecked()
			return to_return
		else:
			return None

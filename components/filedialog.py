from PyQt5.uic import loadUi
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
class FileDialog(QFileDialog):
    def __init__(self, *args,**kwargs):
        super(FileDialog,self).__init__( *args,**kwargs)
    def close(self):
        self.done(1)
    def cancel(self):
        self.done(0)
    def get(self):
        fileName=self.getSaveFileName(self, 'Save File')
        #self.show()
        # if self.exec_():
        #     return fileName
        #
        # else:
        #     return None

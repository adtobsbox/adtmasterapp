from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import json
import logging

class ADTDoubleDialog(QWidget):

    def __init__(self,text,preset=0):
        super().__init__()
        QWidget.__init__(self)
        self.text=text
        self.preset=preset
        if not self.preset:
            self.preset=0.0
    def get(self):
        #dialog=QInputDialog()

        self.num,self.ok = QInputDialog.getDouble(self,"double input dialog",self.text,self.preset)
        if self.ok:
            return self.num
        else:
            return None
        #self.exec_()

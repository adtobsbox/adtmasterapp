from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import numpy as np
from PyQt5 import QtCore, QtWidgets,QtGui
import logging

class WindowProfileCanvas(QtWidgets.QWidget):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.canvas = FigureCanvas(self.fig)
        self.axes = self.fig.add_subplot(111)
        super(WindowProfileCanvas, self).__init__()
        self.x_data=np.linspace(0,3563,3564)
        self.y_data=np.zeros(3564)
        self.axes.plot(self.x_data, self.y_data)
        self.axes.set_xlim([0,3563])
        self.axes.set_ylim([-127,128])
        self.layout=QtWidgets.QVBoxLayout()
        self.windowProfileToolbar = NavigationToolbar(self.canvas, self)
        self.layout.addWidget(self.windowProfileToolbar)
        self.layout.addWidget(self.canvas)
        self.setLayout(self.layout)
    def setBunches(self,startBunch,endBunch):
        if startBunch<0 or startBunch>3564:
            logging.info("startbunch out of bounds")
            return
        if endBunch<0 or endBunch>3564:
            logging.info("endBunch out of bounds")
            return
        if startBunch>endBunch:
            logging.info("startBunch greater and endBunch")
            return
        self.y_data[startBunch:endBunch+1]=128
        self.axes.cla()
        self.axes.plot(self.x_data, self.y_data)
        self.canvas.draw()

    def clearBunches(self,startBunch,endBunch):
        if startBunch<0 or startBunch>3564:
            logging.info("startbunch out of bounds")
            return
        if endBunch<0 or endBunch>3564:
            logging.info("endBunch out of bounds")
            return
        if startBunch>endBunch:
            logging.info("startBunch greater and endBunch")
            return
        self.y_data[startBunch:endBunch+1]=0
        self.axes.cla()
        self.axes.plot(self.x_data, self.y_data)
        self.canvas.draw()

    def clearAllBunches(self):
        self.y_data[0:3564]=0
        self.axes.cla()
        self.axes.plot(self.x_data, self.y_data)
        self.canvas.draw()

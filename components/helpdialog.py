from PyQt5.uic import loadUi
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import os
class HelpDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # Load the dialog's GUI
        DIRNAME = os.path.split(__file__)[0]
        ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/helpdialog.ui")
        loadUi(ui_file, self)
        self.OK.clicked.connect(self.close)
        self.show()
    def close(self):
        self.done(0)

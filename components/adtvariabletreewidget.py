from PyQt5.QtCore import *
from PyQt5.QtGui import QDrag
from PyQt5.QtWidgets import *
import json
import logging
import csv

class ADTVariableTreeWidget(QTreeWidget):

    def __init__(self, parent):
        super().__init__()
        QTreeWidget.__init__(self)
        self.expandAll()
        self.root = self.invisibleRootItem()

    def addVariable(self,unit,variableName,value):
        for i in range(self.root.childCount()):
            if self.root.child(i).text(0)==unit and self.root.child(i).text(1)==variableName:
                self.root.child(i).setText(2,str(value))
                return
        item = QTreeWidgetItem(self.root, [ unit, variableName, str(value)])

    def saveToCSV(self,fileName):
        logging.info("Writing variable table to file "+fileName)
        try:
            with open(fileName, 'w', newline='') as csvfile:
                fieldnames = ['Unit', 'VariableName','Value']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for i in range(self.root.childCount()):
                    writer.writerow({'Unit': self.root.child(i).text(0), 'VariableName': self.root.child(i).text(1),'Value':self.root.child(i).text(2)})
        except IOError:
            logging.error("csv file error")
    def clearVariables(self):
        self.clear()

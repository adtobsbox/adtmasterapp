
import threading
from PyQt5.QtCore import (QCoreApplication, QObject, QRunnable, QThread,
                          QThreadPool, pyqtSignal, pyqtSlot)

class ADTWorker(QThread):
    def __init__(self, function, *args, **kwargs):
        super().__init__()
        QObject.__init__(self)
        self.function = function
        self.args = args
        self.kwargs = kwargs
    def run(self):
        print("thread running")
        self.function(*self.args, **self.kwargs)

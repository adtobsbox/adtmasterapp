import sys
from PyQt5.QtCore import pyqtSignal, QObject, QCoreApplication,QThread
from PyQt5 import QtGui
import logging
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import importlib
import os
import traceback
import components.pyjapcinterface as pyjapc
from components.adtworker import ADTWorker

class SelectedObjects:
    def __init__(self, parent):
        self.beampos=[]
        self.dspu=[]
        self.bunches=[]

class Variables:
    def __init__(self):
        self.variables={}
    def add(self,unit,variableName,value):
        self.variables[(unit,variableName)]=value
    def get(self,unit,variableName):
        if (unit,variableName) in self.variables:
            return self.variables[(unit,variableName)]

class Executable:
    name=""
    description=""
    script=""
    selectable=False
    executable=False

class Logic(QObject):
    newVariable = pyqtSignal(str,str,QVariant)
    clearVariables=pyqtSignal()
    def __init__(self, parent,debug):
        super().__init__()
        QObject.__init__(self)
        self.selectedObject=SelectedObjects(self)
        if debug:
            logging.debug("Logic is running with simulation=True")
        self.japc = pyjapc.PyJapc(simulation=debug)
        self.japc.rbacLogin(username='msoderen', password=None, loginDialog=True)
        self.japc.setSelector('')
        #{unit: (variablename,value) }
        self.variables=Variables()
        self._loaded_modules={}

    def __del__(self):
        del self.japc

    # Defines a signal with two parameters, STR and dataChanged
    # Define Slot Function
    def beamposClicked(self,checked):
        beampos=self.sender().text()
        if checked and beampos not in self.selectedObject.beampos:
            self.selectedObject.beampos.append(beampos)
            logging.info('beampos button: '+beampos+" was added to the list of beampos to work on" )
        elif not checked and beampos in self.selectedObject.beampos:
            self.selectedObject.beampos.remove(beampos)
            logging.info('beampos button: '+beampos+" was removed from the list of beampos to work on" )
        elif not checked and beampos not in self.selectedObject.beampos:
            logging.error('beampos button: '+beampos+" was unchecked but beampos was not in list over beampos to work on, should not happen" )
        elif checked and beampos in self.selectedObject.beampos:
            logging.error('beampos button: '+beampos+" was checked but beampos was already in list over beampos to work on, should not happen" )

    def dspuClicked(self,checked):
        dspu=self.sender().text()
        if checked and dspu not in self.selectedObject.dspu:
            self.selectedObject.dspu.append(dspu)
            logging.info('dspu button: '+dspu+" was added to the list of dspu to work on" )
        elif not checked and dspu in self.selectedObject.dspu:
            self.selectedObject.dspu.remove(dspu)
            logging.info('dspu button: '+dspu+" was removed from the list of dspu to work on" )
        elif not checked and dspu not in self.selectedObject.dspu:
            logging.error('dspu button: '+dspu+" was unchecked but dspu was not in list over dspu to work on, should not happen" )
        elif checked and dspu in self.selectedObject.dspu:
            logging.error('dspu button: '+dspu+" was checked but dspu was already in list over dspu to work on, should not happen" )
    def clearVariablesSlot(self):
        self.variables=Variables()
        self.clearVariables.emit()

    def confirmationClicked(self,checked):
        self.japc.setConfirmation(checked)

    def noSetClicked(self,checked):
        self.japc.setNoSet(checked)

    def nodeToExecutable(self,node):
        tempExecutable=Executable()
        tempExecutable.name=node.text(0)
        tempExecutable.description=node.text(1)
        tempExecutable.script=node.text(2)
        tempExecutable.selectable= node.text(3)=="True"
        tempExecutable.executable= node.text(4)=="True"
        return tempExecutable

    #    script can return [(unit,variablename,value)]
    def checkReturnData(self,data):
        if type(data)==list and set(map(type, data)) == {tuple}:
            for element in data:
                if len(element)==3:
                    unit=element[0]
                    variableName=element[1]
                    value=element[2]
                    logging.info("Adding variable " +unit+" "+variableName+" "+str(value))
                    self.variables.add(unit,variableName,value)
                    self.newVariable.emit(unit,variableName,QVariant(value))


    #script can return [(unit,variablename,value)]
    def runNode(self,node,step):
        executable=self.nodeToExecutable(node)
        if executable.executable:
            logging.info("Running node "+executable.script)
            #get module to import
            module=os.path.splitext(executable.script)[0]
            logging.info("importing "+module)
            try:
                m1=None
                if module not in self._loaded_modules:
                    m1 = importlib.import_module('scripts.'+module)
                    self._loaded_modules[module]=m1
                else:
                    m1=importlib.reload(self._loaded_modules[module])
                    self._loaded_modules[module]=m1
            except ModuleNotFoundError as e:
                logging.error("Module not found error, could be becuase the script does not exists or because the script requires imports that were  not found")
                logging.error(str(e))
                node.setBackground(0,QColor('red'))
                return
            except SyntaxError as e:
                logging.error("SyntaxError in module")
                logging.error(str(e))
                node.setBackground(0,QColor('red'))
                return
            except Exception as e:
                logging.error("Script "+module+" threw exception while trying to import it: "+traceback.format_exc())
                logging.error(str(e))
                node.setBackground(0,QColor('red'))
                return
            try:
                logging.info("Running script: "+module )
                returnData=m1.run(self.japc,self.selectedObject,self.variables)
                #worker=ADTWorker(m1.run,self.japc,self.selectedObject,self.variables)
                #worker.start()
                #worker.wait()
                #returnData=ADTWorker(m1.run,self.japc,self.selectedObject,self.variables)
                self.checkReturnData(returnData)
                if (type(returnData)==bool and returnData==False) or returnData==None:
                    node.setBackground(0,QColor('red'))
                else:
                    node.setBackground(0,QColor('green'))
            except Exception as e:
                logging.error("Script "+module+" threw exception: "+traceback.format_exc())
                #could be that the exception comes from the application setting the color of a node that has been deleted
                if "QTreeWidgetItem" not in str(e):
                    node.setBackground(0,QColor('red'))
        else:
            node.setBackground(0,QColor('green'))
        QApplication.processEvents()
        if step:
            return
        for i in range(node.childCount()):
            self.runNode(node.child(i),step)


    def run(self,root,step):
        logging.info("starting to run scripts")
        self.runNode(root,step)
        # for i in range(root.childCount()):
        #     self.runNode(root.child(i))

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import numpy as np
from PyQt5 import QtCore, QtWidgets,QtGui
import logging

#FigureCanvas
class ExcitationLengthProfileCanvas(QtWidgets.QWidget):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.canvas = FigureCanvas(self.fig)
        self.axes = self.fig.add_subplot(111)
        super(ExcitationLengthProfileCanvas, self).__init__()
        self.x_data=np.linspace(0,9999,10000)
        self.y_data=np.zeros(10000)
        self.axes.plot(self.x_data, self.y_data)
        self.axes.set_xlim([0,10000])
        self.axes.set_ylim([0,2**15])
        self.layout=QtWidgets.QVBoxLayout()
        self.windowProfileToolbar = NavigationToolbar(self.canvas, self)
        self.layout.addWidget(self.windowProfileToolbar)
        self.layout.addWidget(self.canvas)
        self.setLayout(self.layout)
    def update(self,riseTurns,flattopTurns,fallTurns,skipTurns,amplitude):
        x=np.linspace(0,riseTurns+flattopTurns+fallTurns,riseTurns+flattopTurns+fallTurns+1)
        xp=np.array([0,riseTurns,riseTurns+flattopTurns,riseTurns+flattopTurns+fallTurns])
        fp=np.array([0,1,1,0])
        self.x_data=x
        self.y_data=np.interp(x,xp,fp)
        self.y_data*=32768.0*amplitude
        self.axes.cla()
        self.axes.plot(self.x_data, self.y_data)
        self.canvas.draw()

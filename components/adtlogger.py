from PyQt5.QtCore import *
from PyQt5.QtGui import QDrag
from PyQt5.QtWidgets import *
import PyQt5
from PyQt5 import QtGui

import logging


class CustomFormatter(logging.Formatter):
    FORMATS = {
        logging.ERROR:   ("%(asctime)s [%(levelname)-8s] %(message)s", QtGui.QColor("red")),
        logging.DEBUG:   ("%(asctime)s [%(levelname)-8s] [%(filename)s:%(lineno)d] %(message)s", QtGui.QColor("blue")),
        logging.INFO:    ("%(asctime)s [%(levelname)-8s] %(message)s", QtGui.QColor("green")),
        logging.WARNING: ('%(asctime)s - %(name)s - %(levelname)s - %(message)s', QtGui.QColor("yellow"))
    }

    def format( self, record ):
        last_fmt = self._style._fmt
        opt = CustomFormatter.FORMATS.get(record.levelno)
        if opt:
            fmt, color = opt
            self._style._fmt = "<font color=\"{}\">{}</font>".format(QtGui.QColor(color).name(),fmt)
        res = logging.Formatter.format( self, record )
        self._style._fmt = last_fmt
        return res

class ADTLogger(logging.Handler, QPlainTextEdit):
    def __init__(self, parent):
        super().__init__()
        QPlainTextEdit.__init__(self)
        self.setReadOnly(True)
        self.setFormatter(CustomFormatter())
        logging.getLogger().addHandler(self)
        logging.getLogger().setLevel(logging.DEBUG)



    def emit(self, record):
        msg = self.format(record)

        #self.appendHtml("<font color='red' ><red>Hello PyQt5!\nHello</font>")
        self.appendHtml(msg)
        self.moveCursor(QtGui.QTextCursor.End)

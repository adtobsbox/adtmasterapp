# -*- coding: utf-8 -*-
#Author Martin Soderen
import os
import h5py
import numpy as np
rootpath="/nfs/cs-ccr-adtobsnfs/lhc_adtobsbox_data"
#For easy structuring
class Dataset:
  def  __init__(self,group,dataset,data):
    self.group=group
    self.dataset=dataset
    self.data=data
    self.rows=data.shape[1]
    self.cols=data.shape[0]
  def __str__(self):
    return self.group+"/"+self.dataset+": rows: "+str(self.rows)+" cols: "+str(self.cols)
  def __getitem__(self,index):
    return self.data[index]

class PlotFile:
  def __init__(self,filename):
    self.open=False
    self.filename=filename
    if not os.path.isfile(self.filename):
      return
    try:
      self.file=h5py.File(self.filename,'r')
    except OSError:
      return
    #extract fillnumber
    self.fill=self.filename.split("_")[-6].split("/")[-1]
    if self.fill[0]=="0":
      self.fill=self.fill[1:]
    #create real filename
    self.justfilename=self.filename.split("/")[-1].split(".h5")[0]+".png"
    #extract all datasets
    self.datasets=[]
    for group in self.file.keys():
      for dataset in self.file[group].keys():
        if dataset=="attributes":
          continue
        data=np.transpose(self.file[group][dataset])
        if data.shape[0]>0 and data.shape[1]>0:
          self.datasets.append(Dataset(group,dataset,data))
    self.open=True

  def __getitem__(self,index):
    return self.datasets[index]

  def close(self):
    self.file.flush()
    self.file.close()

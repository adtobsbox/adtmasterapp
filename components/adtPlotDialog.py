import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from components.genericPlot import GenericPlot
import logging


class ADTPlotDialog(QtWidgets.QDialog):
	def __init__(self,parent=None,x=1,y=1):
		super().__init__(parent)
		self._plots={}
		self.grid_layout = QtWidgets.QGridLayout()
		for x_coord in range(x):
			for y_coord in range(y):
				plot=GenericPlot()
				self._plots["plot_"+str(x_coord)+"_"+str(y_coord)]=plot
				self.grid_layout.addWidget(plot, x_coord, y_coord, 1, 1)
		self.setLayout(self.grid_layout)
	def run(self):
		self.show()
		self.exec_()
from PyQt5.QtCore import *
from PyQt5.QtGui import QDrag,QKeySequence,QColor
from PyQt5.QtWidgets import *
import json
import sip
from components.adtmessagedialog import ADTMessageDialog
class ADTTreeWidget(QTreeWidget):
    customMimeType = "application/x-customTreeWidgetdata"
    runEvent = pyqtSignal(QTreeWidgetItem,bool)
    doubleClickEvent = pyqtSignal(QTreeWidgetItem)

    def __init__(self, parent):
        super().__init__()
        QTreeWidget.__init__(self)
        self.expandAll()
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setDragEnabled(True)
        self.viewport().setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.root = self.invisibleRootItem()
        self.dragTo = True
        self.dragFrom = True
        self.steps=[]
        self.currentStep=0
        self.itemDoubleClicked.connect(self.doubleClickHandler)
        #self.installEventFilter(self)
    def resetBackground(self,node):
        node.setBackground(0,QColor('white'))
        for i in range(node.childCount()):
                self.resetBackground(node.child(i))

    def run(self):
        self.resetBackground(self.root)
        self.runEvent.emit(self.root,False)
    def traverseSteps(self,node):
        self.steps.append(node)
        for i in range(node.childCount()):
            self.traverseSteps(node.child(i))

    def runStep(self):
        if self.currentStep==0:
            dialog=ADTMessageDialog("You want to start stepping?")
            val=dialog.get()
            if not val:
                return
            self.steps=[]
            self.traverseSteps(self.root)
            #skip first invisible node
            self.steps=self.steps[1:]
            self.resetBackground(self.root)
        if self.currentStep>=len(self.steps):
            self.currentStep=0
            return
        self.runEvent.emit(self.steps[self.currentStep],True)
        self.currentStep+=1

    def resetSteps(self):
        self.steps=[]
        self.currentStep=0
        self.resetBackground(self.root)
    # def eventFilter(self, object, event):
    #     print(event.type())
    #     if event.type()==QEvent.ShortcutOverride:
    #         if event.key() == Qt.Key_Delete:
    #             print("delete key")
    #             return True
    #     return False

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Delete:
            self.deleteSelected()
            event.accept()

    def deleteSelected(self):
        if self.dragFrom:
            return
        for element in self.selectedItems():
            if element.text(3)=="True":
                sip.delete(element)

    def mimeTypes(self):
        mimetypes = QTreeWidget.mimeTypes(self)
        mimetypes.append(ADTTreeWidget.customMimeType)
        return mimetypes

    def loadJson(self, filename):
        with open(filename) as json_file:
            data = json.load(json_file)
            for element, value in data.items():
                self.addNode(self.root,element,value)

    def doubleClickHandler(self,item,columnNumber):
        if self.dragTo:
            self.deleteSelected()
            return
        if item.text(3)=="True":
            self.doubleClickEvent.emit(item)

    def addItem(self, item):
        newItem = QTreeWidgetItem(self)
        self.fillItem(item, newItem)
        self.fillItems(item, newItem)

    def addNode(self,root, element,value):
        root = QTreeWidgetItem(root, [element])
        root.setFlags(Qt.ItemIsEnabled)
        for subelement in value:
            if type(subelement["script"])==str:
                item = QTreeWidgetItem(root, [ subelement["name"], subelement["description"], subelement["script"], subelement["selectable"],subelement["executable"]])
                #item.setBackground(0,QColor('green'))
            else:
                item = QTreeWidgetItem(root, [ subelement["name"], subelement["description"], "", subelement["selectable"],subelement["executable"]])
                self.addChild(item, "script",subelement["script"])
            if subelement["selectable"] == "True":
                item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDragEnabled)
            else:
                item.setFlags(Qt.ItemIsEnabled)





    def addChild(self,root, element,value):
        for subelement in value:
            if type(subelement["script"])==str:
                item = QTreeWidgetItem(root, [ subelement["name"], subelement["description"], subelement["script"], subelement["selectable"],subelement["executable"]])
            else:
                item = QTreeWidgetItem(root, [ subelement["name"], subelement["description"], "", subelement["selectable"],subelement["executable"]])
                self.addNode(item, "script",subelement["script"])
            if subelement["selectable"] == "True":
                item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDragEnabled)
            else:
                item.setFlags(Qt.ItemIsEnabled)
    def startDrag(self, supportedActions):
        if not self.dragFrom:
            self.deleteSelected()
            return
        drag = QDrag(self)
        mimedata = self.model().mimeData(self.selectedIndexes())
        encoded = QByteArray()
        stream = QDataStream(encoded, QIODevice.WriteOnly)
        self.encodeData(self.selectedItems(), stream)
        mimedata.setData(ADTTreeWidget.customMimeType, encoded)

        drag.setMimeData(mimedata)
        drag.exec_(supportedActions)

    def dropEvent(self, event):
        if not self.dragTo:
            return
        if event.source() == self:
            pass
            # event.setDropAction(Qt.MoveAction)
            #QTreeWidget.dropEvent(self, event)
        elif isinstance(event.source(), QTreeWidget):
            if event.mimeData().hasFormat(ADTTreeWidget.customMimeType):
                encoded = event.mimeData().data(ADTTreeWidget.customMimeType)
                parent = self.itemAt(event.pos())
                if parent == None:
                    parent = self.root
                items = self.decodeData(encoded, event.source())
                for it in items:
                    item = QTreeWidgetItem(parent)
                    self.fillItem(it, item)
                    self.fillItems(it, item)
                event.acceptProposedAction()



    def fillItem(self, inItem, outItem):
        for col in range(inItem.columnCount()):
            for key in range(Qt.UserRole):
                role = Qt.ItemDataRole(key)
                outItem.setData(col, role, inItem.data(col, role))

    def fillItems(self, itFrom, itTo):
        for ix in range(itFrom.childCount()):
            it = QTreeWidgetItem(itTo)
            ch = itFrom.child(ix)
            self.fillItem(ch, it)
            self.fillItems(ch, it)

    def encodeData(self, items, stream):
        stream.writeInt32(len(items))
        for item in items:
            p = item
            rows = []
            while p is not None:
                rows.append(self.indexFromItem(p).row())
                p = p.parent()
            stream.writeInt32(len(rows))
            for row in reversed(rows):
                stream.writeInt32(row)
        return stream

    def decodeData(self, encoded, tree):
        items = []
        rows = []
        stream = QDataStream(encoded, QIODevice.ReadOnly)
        while not stream.atEnd():
            nItems = stream.readInt32()
            for i in range(nItems):
                path = stream.readInt32()
                row = []
                for j in range(path):
                    row.append(stream.readInt32())
                rows.append(row)

        for row in rows:
            it = tree.topLevelItem(row[0])
            for ix in row[1:]:
                it = it.child(ix)
            items.append(it)
        return items

import logging
import numpy as np
from components.adtmessagedialog import ADTMessageDialog

_INSTANCE_DEFAULT = object()

class PyJapc():
    def __init__(
        self,
        selector="LHC.USER.ALL",
        incaAcceleratorName="auto",
        noSet=False,
        timeZone="utc",
        logLevel=None,
        simulation=False
    ) -> None:
        self.simulation=simulation
        self.confirmation=False
        self.noSet=False
        if not self.simulation:
            import pyjapc
            self.pyjapc=pyjapc.PyJapc(selector=selector,incaAcceleratorName=incaAcceleratorName,noSet=noSet,timeZone=timeZone,logLevel=logLevel)
    
    def __del__(self):
        if not self.simulation:
            self.pyjapc.clearSubscriptions()
            self.pyjapc.rbacLogout()
            del self.pyjapc

    def setConfirmation(self,confirmation):
        logging.debug("Asking for confirmation before set it set to: "+str(confirmation))
        self.confirmation=confirmation

    def setNoSet(self,noSet):
        self.noSet=noSet
        logging.debug("No set is set to: "+str(noSet))
        if not self.simulation:
            self.pyjapc._noSet=noSet


    def rbacLogin(self, username=None, password=None, loginDialog=False, readEnv=True):
        if self.simulation:
            return
        self.pyjapc.rbacLogin(username=username,password=password,loginDialog=loginDialog,readEnv=readEnv)

    def setSelector(self, timingSelector, dataFilter=None):
        if self.simulation:
            return
        self.pyjapc.setSelector(timingSelector=timingSelector,dataFilter=dataFilter)

    def setParam(self,*args, **kwargs):
        if "parameterName" in kwargs.keys():
            parameterName=kwargs["parameterName"]
        else:
            parameterName=args[0]
        if "parameterValue" in kwargs.keys():
            parameterValue=kwargs["parameterValue"]
        else:
            parameterValue=args[1]
        if self.confirmation:
            dialog=ADTMessageDialog("Setting "+parameterName+" to: "+str(parameterValue) )
            if not dialog.get():
                logging.info("did not set "+parameterName+" to "+str(parameterValue)+" because used cancelled")
                return

        if self.simulation:
            logging.debug("Setting simulated parameter "+parameterName+" to "+str(parameterValue) )
            return
        logging.debug("Setting parameter "+parameterName+" to "+str(parameterValue) )
        self.pyjapc.setParam(*args, **kwargs)


    def getParam(self,*args, **kwargs):
        if self.simulation:
            if "buffer" in args[0]:
                if "dataFilterOverride" in kwargs.keys():
                    if "size" in kwargs["dataFilterOverride"].keys():
                        return np.random.normal(100,10,kwargs["dataFilterOverride"]["size"])
                return np.random.normal(0,5,4096)+100.0
            if "flags" in args[0]:
                if "dataFilterOverride" in kwargs.keys():
                    if "size" in kwargs["dataFilterOverride"].keys():
                        data=np.zeros(kwargs["dataFilterOverride"]["size"])
                        data[1000]=3
                        return data
                return np.zeros(4096)
            return
        return self.pyjapc.getParam(*args, **kwargs)

    def subscribeParam(self,*args, **kwargs):
        if "parameterName" in kwargs.keys():
            parameterName=kwargs["parameterName"]
        else:
            parameterName=args[0]

        if self.simulation:
            logging.debug("Simulated subscription to parameter "+parameterName )
            return
        logging.debug("Subscription to parameter "+parameterName+" setup " )
        self.pyjapc.subscribeParam(*args, **kwargs)

    def startSubscriptions(self, parameterName=None, selector=None):
        if self.simulation:
            logging.debug("Starting subscription " )
            return
        logging.debug("Starting Subscription" )
        self.pyjapc.startSubscriptions(parameterName, selector)

    def clearSubscriptions(self, parameterName=None, selector=None):
        if self.simulation:
            logging.debug("Clearing subscription " )
            return
        logging.debug("Clearing Subscription" )
        self.pyjapc.clearSubscriptions(parameterName, selector)

    def stopSubscriptions(self, parameterName=None, selector=None):
        if self.simulation:
            logging.debug("Stopping subscription " )
            return
        logging.debug("Stopping Subscription" )
        self.pyjapc.stopSubscriptions(parameterName, selector)
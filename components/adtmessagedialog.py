from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import json
import logging

class ADTMessageDialog(QMessageBox):

    def __init__(self,text):
        super().__init__()
        QMessageBox.__init__(self)
        self.setText(text)
        #self.setStandardButtons(  QMessageBox.Cancel | QMessageBox.Ok)
        #self.addButton(QMessageBox.Ok)
        #self.addButton(QMessageBox.Cancel)
        button= self.addButton("OK", QMessageBox.NoRole)
        self.addButton("Cancel", QMessageBox.YesRole)
        self.setDefaultButton(button)
    def get(self):
        return not self.exec_()

from components.logic import SelectedObjects,Variables
from components.adtintdialog import ADTIntDialog
def bitToInt(bits):
    if type(bits)!=list:
        return None
    val=0
    for bit in bits:
        val+=2**bit
    return val

def getBunchesBeampos(selectedObjects: SelectedObjects,variables : Variables):
    planes=[]
    for beampos in selectedObjects.beampos:
        if "Hor" in beampos and "B1" in beampos:
            planes.append("B1H")
        if "Ver" in beampos and "B1" in beampos:
            planes.append("B1V")
        if "Hor" in beampos and "B2" in beampos:
            planes.append("B2H")
        if "Ver" in beampos and "B2" in beampos:
            planes.append("B2V")
    bunches=[]
    B1V_bunch=0
    if  variables.get("","B1V_bunch")!=None:
        B1V_bunch=variables.get("","B1V_bunch")
    if "B1V" in planes:
        dialog=ADTIntDialog("B1V bunch",B1V_bunch)
        B1V_bunch=dialog.get()
        if B1V_bunch!=None:
        	bunches.append(("","B1V_bunch",B1V_bunch))

    
    B1H_bunch=0
    if variables.get("","B1H_bunch")!=None:
        B1H_bunch=variables.get("","B1H_bunch")
    if "B1H" in planes:
        dialog=ADTIntDialog("B1H bunch",B1H_bunch)
        B1H_bunch=dialog.get()
        if B1H_bunch!=None:
        	bunches.append(("","B1H_bunch",B1H_bunch))
    
    B2V_bunch=0
    if variables.get("","B2V_bunch")!=None:
        B2V_bunch=variables.get("","B2V_bunch")
    if "B2V" in planes:
        dialog=ADTIntDialog("B2V bunch",B2V_bunch)
        B2V_bunch=dialog.get()
        if B2V_bunch!=None:
        	bunches.append(("","B2V_bunch",B2V_bunch))
    
    B2H_bunch=0
    if variables.get("","B2H_bunch")!=None:
        B2H_bunch=variables.get("","B2H_bunch")
    if "B2H" in planes:
        dialog=ADTIntDialog("B2H bunch",B2H_bunch)
        B2H_bunch=dialog.get()
        if B2H_bunch!=None:
        	bunches.append(("","B2H_bunch",B2H_bunch))
    return bunches

from PyQt5 import QtWidgets, uic,QtGui
from PyQt5.QtWidgets import  QShortcut
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import sys
import logging
import time
import os
import components
from components import logic,helpdialog
import matplotlib
matplotlib.use('Qt5Agg')
import argparse
import os

uic.properties.logger.setLevel(logging.WARNING)
uic.uiparser.logger.setLevel(logging.WARNING)

class Ui(QtWidgets.QMainWindow):
    runEventSignal = pyqtSignal()
    runStepEventSignal = pyqtSignal()
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        DIRNAME = os.path.split(__file__)[0]
        ui_file = os.path.join(DIRNAME, "ui/mainwindow.ui")
        uic.loadUi(ui_file, self) # Load the .ui file
        self.runSequence = QShortcut(QKeySequence('Ctrl+R'), self)
        self.runSequence.activated.connect(self.runEvent)

        self.runStep = QShortcut(QKeySequence('Ctrl+S'), self)
        self.runStep.activated.connect(self.runStepEvent)

        self.actionHelp.triggered.connect(self.openHelpDialog)
        self.actionSelect_all_beampos.triggered.connect(self.selectAllBeampos)
        self.actionDeselect_all_beampos.triggered.connect(self.deselectAllBeampos)
        self.actionSelect_all_DSPU.triggered.connect(self.selectAllDSPU)
        self.actionDeselect_all_DSPU.triggered.connect(self.deselectAllDSPU)
        self.actiondump_log_to_file.triggered.connect(self.writeLogToFile)
        self.actiondump_variable_table.triggered.connect(self.writeVariableTableToFile)
        self.statusbar.showMessage("Developed by Martin Söderén (SY-RF-FB)");
        self.show() # Show the GUI
    def closeEvent(self,event):
        event.accept()
    def runEvent(self):
        logging.debug("triggering runevent by ctrl+R")
        self.runEventSignal.emit()
    def runStepEvent(self):
        logging.debug("triggering runStepEvent by ctrl+R")
        self.runStepEventSignal.emit()
    def openHelpDialog(self):
        dialog=helpdialog.HelpDialog(self)
    def setButtonState(self,button,state):
        if not button.isChecked()==state:
            button.animateClick()
    def selectAllBeampos(self):
        self.setButtonState(self.ADTBposHorQ7LB1,True)
        self.setButtonState(self.ADTBposHorQ8RB1,True)
        self.setButtonState(self.ADTBposHorQ9LB1,True)
        self.setButtonState(self.ADTBposHorQ10RB1,True)
        self.setButtonState(self.ADTBposVerQ7RB1,True)
        self.setButtonState(self.ADTBposVerQ8LB1,True)
        self.setButtonState(self.ADTBposVerQ9RB1,True)
        self.setButtonState(self.ADTBposVerQ10LB1,True)
        self.setButtonState(self.ADTBposHorQ7RB2,True)
        self.setButtonState(self.ADTBposHorQ8LB2,True)
        self.setButtonState(self.ADTBposHorQ9RB2,True)
        self.setButtonState(self.ADTBposHorQ10LB2,True)
        self.setButtonState(self.ADTBposVerQ7LB2,True)
        self.setButtonState(self.ADTBposVerQ8RB2,True)
        self.setButtonState(self.ADTBposVerQ9LB2,True)
        self.setButtonState(self.ADTBposVerQ10RB2,True)
    def deselectAllBeampos(self):
        self.setButtonState(self.ADTBposHorQ7LB1,False)
        self.setButtonState(self.ADTBposHorQ8RB1,False)
        self.setButtonState(self.ADTBposHorQ9LB1,False)
        self.setButtonState(self.ADTBposHorQ10RB1,False)
        self.setButtonState(self.ADTBposVerQ7RB1,False)
        self.setButtonState(self.ADTBposVerQ8LB1,False)
        self.setButtonState(self.ADTBposVerQ9RB1,False)
        self.setButtonState(self.ADTBposVerQ10LB1,False)
        self.setButtonState(self.ADTBposHorQ7RB2,False)
        self.setButtonState(self.ADTBposHorQ8LB2,False)
        self.setButtonState(self.ADTBposHorQ9RB2,False)
        self.setButtonState(self.ADTBposHorQ10LB2,False)
        self.setButtonState(self.ADTBposVerQ7LB2,False)
        self.setButtonState(self.ADTBposVerQ8RB2,False)
        self.setButtonState(self.ADTBposVerQ9LB2,False)
        self.setButtonState(self.ADTBposVerQ10RB2,False)
    def selectAllDSPU(self):
        self.setButtonState(self.ADTmDSPUHorM1B1,True)
        self.setButtonState(self.ADTmDSPUHorM2B1,True)
        self.setButtonState(self.ADTmDSPUVerM1B1,True)
        self.setButtonState(self.ADTmDSPUVerM2B1,True)
        self.setButtonState(self.ADTmDSPUHorM1B2,True)
        self.setButtonState(self.ADTmDSPUHorM2B2,True)
        self.setButtonState(self.ADTmDSPUVerM1B2,True)
        self.setButtonState(self.ADTmDSPUVerM2B2,True)
    def deselectAllDSPU(self):
        self.setButtonState(self.ADTmDSPUHorM1B1,False)
        self.setButtonState(self.ADTmDSPUHorM2B1,False)
        self.setButtonState(self.ADTmDSPUVerM1B1,False)
        self.setButtonState(self.ADTmDSPUVerM2B1,False)
        self.setButtonState(self.ADTmDSPUHorM1B2,False)
        self.setButtonState(self.ADTmDSPUHorM2B2,False)
        self.setButtonState(self.ADTmDSPUVerM1B2,False)
        self.setButtonState(self.ADTmDSPUVerM2B2,False)
    def writeLogToFile(self):
        name = QFileDialog.getSaveFileName(self, 'Save File')
        file = open(name[0],'w')
        text = self.plainTextEdit.toPlainText()
        file.write(text)
        file.close()

    def writeVariableTableToFile(self):
        name = QFileDialog.getSaveFileName(self, 'Save File')
        self.variables.saveToCSV(name[0])

def main(debug):
    app = QtWidgets.QApplication(sys.argv)
    window = Ui()
    DIRNAME = os.path.split(__file__)[0]
    icon_file = os.path.join(DIRNAME, "resources/icon.png")
    window.setWindowIcon(QtGui.QIcon(icon_file))
    window.setWindowTitle("ADT Master Control Application")
    DIRNAME = os.path.split(__file__)[0]
    json_file = os.path.join(DIRNAME, "scripts.json")
    window.selectable.loadJson(json_file)
    window.selectable.dragTo=False
    window.selected.dragFrom=False
    logicManager=logic.Logic(window,debug)
    #all beampos buttons
    window.ADTBposHorQ7LB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ8RB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ9LB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ10RB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ7RB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ8LB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ9RB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ10LB1.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ7RB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ8LB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ9RB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposHorQ10LB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ7LB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ8RB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ9LB2.clicked.connect(logicManager.beamposClicked)
    window.ADTBposVerQ10RB2.clicked.connect(logicManager.beamposClicked)
    #beampos test devices
    window.ADTBposTestQ7_adttestn.clicked.connect(logicManager.beamposClicked)
    window.ADTBposTestQ8_adttestn.clicked.connect(logicManager.beamposClicked)
    window.ADTBposTestQ9_adttestn.clicked.connect(logicManager.beamposClicked)
    window.ADTBposTestQ10_adttestn.clicked.connect(logicManager.beamposClicked)
    window.ADTBeamPos1_alab4.clicked.connect(logicManager.beamposClicked)
    window.ADTBeamPos2_alab4.clicked.connect(logicManager.beamposClicked)
    window.ADTBeamPos3_alab4.clicked.connect(logicManager.beamposClicked)
    window.ADTBeamPos4_alab4.clicked.connect(logicManager.beamposClicked)

    #all DSPUs
    window.ADTmDSPUHorM1B1.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUHorM2B1.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUVerM1B1.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUVerM2B1.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUHorM1B2.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUHorM2B2.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUVerM1B2.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUVerM2B2.clicked.connect(logicManager.dspuClicked)
    #dspu test devices
    window.ADTmDSPUTest0.clicked.connect(logicManager.dspuClicked)
    window.ADTmDSPUTest1.clicked.connect(logicManager.dspuClicked)

    logicManager.newVariable.connect(window.variables.addVariable)

    window.actionAsk_for_confirmation_before_set.triggered.connect(logicManager.confirmationClicked)
    window.actionno_set.triggered.connect(logicManager.noSetClicked)

    window.actionClear_variable_table.triggered.connect(logicManager.clearVariablesSlot)
    logicManager.clearVariables.connect(window.variables.clearVariables)

    window.run.clicked.connect(window.selected.run)
    window.runEventSignal.connect(window.selected.run)

    window.selected.runEvent.connect(logicManager.run)

    window.step.clicked.connect(window.selected.runStep)
    window.runStepEventSignal.connect(window.selected.runStep)
    window.reset_steps.clicked.connect(window.selected.resetSteps)

    window.selectable.doubleClickEvent.connect(window.selected.addItem)
    returnVal=app.exec_()
    del logicManager
    sys.exit(returnVal)
    



if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument("-test","-t", help="runs a test environment, if set then pyjapc uses a basic simulation class instead",action="store_true")
    args = parser.parse_args()
    main(args.test)

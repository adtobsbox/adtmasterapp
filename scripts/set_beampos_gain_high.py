import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
from components.bitdialog import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    set=False
    for beampos in selectedObjects.beampos:
        for gain in ["gainDelta","gainSum"]:
            old_value=japc.getParam(beampos+'/HighSettings#'+gain)
            temp_old_value=0
            if type(old_value)!=int:
                for element in old_value:
                    temp_old_value+=element[0]
                old_value=temp_old_value
            bitdialog=BitDialog(gain+" high for module "+beampos,{0:"0.5 dB pad",1:"1 dB pad",2:"2 dB pad",3:"4 dB pad",4:"8 dB pad",6:"amplifier in",7:"amplifier off"},old_value)
            val= bitdialog.get()
            if val!=None:
                logging.info("Setting beampos high intensity gain for module "+beampos+" to "+str(val))
                japc.setParam(beampos+'/HighSettings#'+gain,val)
                set=True
            else:
                logging.info("cancelling setting of beampos high intensity gain for module "+beampos+" because of user")
    return set

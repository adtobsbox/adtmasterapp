#import pyjapc
import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
from components.adtintdialog import ADTIntDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
from numpy import pi as pi
import time

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running plot_raw_adc_data_with_tags for beampos "+beampos)
        bufsize = 10692
        offset=25
        raw = False
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256

        buffer13 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer13_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset})
        buffer14 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+13,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer15 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+14,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer16 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+15,'raw': raw, 'size':bufsize,'offset':offset}))
        dialog=ADTIntDialog("Set bunch",0)
        bunch=dialog.get()
        index = np.where(buffer13_tag == 3)[0][0]
        print(buffer13_tag)
        logging.info("frev tag index: "+str(index))
        if bunch!=None:
            index=index+bunch*3
        logging.info("bunch index: "+str(index))
        AngleSum = np.arctan2(buffer14[index],buffer13[index])*180/pi
        MagSum = np.sqrt(buffer14[index]**2 + buffer13[index]**2)
        AngleDelta = np.arctan2(buffer16[index],buffer15[index])*180/pi
        MagDelta = np.sqrt(buffer16[index]**2 + buffer15[index]**2)
        toReturn=[]
        toReturn.append((beampos,"AngleSum",AngleSum))
        toReturn.append((beampos,"MagSum",MagSum))
        toReturn.append((beampos,"AngleDelta",AngleDelta))
        toReturn.append((beampos,"MagDelta",MagDelta))
    if toReturn:
        return toReturn
    return False

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	good=True
	for manager in ["ObsBox2Manager.LHC.ADT.DEV.B2","ObsBox2Manager.LHC.ADT.DEV.B1","ObsBox2Manager.LHC.ADT.REAL.B1","ObsBox2Manager.LHC.ADT.REAL.B2","ObsBox2Manager.LHC.ADT.BUF.B2","ObsBox2Manager.LHC.ADT.BUF.B1","ObsBox2Manager.LHC.ADT.REAL.mDSPU"]:
		japc.setParam(manager+'/reset',{})
	return True
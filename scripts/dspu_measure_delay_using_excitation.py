import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
from components.adtintdialog import ADTIntDialog
from components.adtFileStorage import adtFileStorage
import logging
from enum import Enum
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog
from PyQt5 import QtGui
from PyQt5.uic import loadUi
from PyQt5.QtCore import QTimer,pyqtSignal
import os
import numpy as np
import traceback
import h5py
logging.getLogger('matplotlib.font_manager').disabled = True
class State(Enum):
	SettingUp=0
	FirstExcitation=1
	SecondExcitation=2

class ExcitationMeasurement(QDialog):
	checkReadyToRock = pyqtSignal()
	def __init__(self,dspu:str,japc: pyjapc.PyJapc,parent=None):
		super().__init__(parent)
		DIRNAME = os.path.split(__file__)[0]
		ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/dspu_delay_measurement.ui")
		loadUi(ui_file, self)
		self.dspu=dspu
		self.plane=""
		if "Hor" in self.dspu:
			self.plane="Hor"
		elif "Ver" in self.dspu:
			self.plane="Ver"
		self.japc=japc
		self.state=State.SettingUp

		#parameters for excitation
		self.mode=3 # 0=off, 1=pulse,2=cosine,3=sine
		self.N=1 #number of periods per 1 turn excitaiton
		self.frev=11245 #Hz
		self.fS=self.frev*3564*3 # 120MHz clock
		self.FTW=self.N*self.frev*(2**32)/self.fS# frequency tuning word
		self.amplitude=0.9*(2**14)
		self.ExcitationA=1
		self.ExcitationB=0
		self.bunches=3563
		self.Nh=3564
		self.nb=range(0,self.Nh)
		self.nt=range(0)
		self.K=0
		self.sub=range(7,15)

		#insert one bunch to test
		self.test=False
		self.test_phase=0.0

		#setup observation
		#												Pos1			Pos2				Pos3				Pos4			ExcitationA 	ExcitationB 
		self.japc.setParam(self.dspu+'/BufferSelectOBS',{"obsBufferSel1":0,"obsBufferSel2":1,"obsBufferSel3":2,"obsBufferSel4":3,"obsBufferSel5":24,"obsBufferSel6":26})
		#																ExcTrig
		self.japc.setParam(self.dspu+'/BufferSetting',{"tagEnable":1,"trigSel":2**23},dataFilterOverride={'channelName':0})

		#setup excitation
		self.japc.setParam(self.dspu+'/Excitation',{"Dds1ExcFTW":self.FTW,"Dds1ExcAmplitude":self.amplitude})
		self.japc.setParam(self.dspu+'/GuruVolatileControl',{"test1":self.mode,"test2":self.bunches})
		self.japc.setParam(self.dspu+'/GuruControl',{"ttuControl2":self.ExcitationA*1+2**8+self.ExcitationB*2+2**9,})

		#keep track of state of buffer
		self.pos1Good=False
		self.pos2Good=False
		self.pos3Good=False
		self.pos4Good=False
		self.ExcitationAGood=False
		self.ExcitationBGood=False

		#data from buffers
		self.pos1data_sine={"name":"pos1_sine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.pos2data_sine={"name":"pos2_sine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.pos3data_sine={"name":"pos3_sine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.pos4data_sine={"name":"pos4_sine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.excitationAdata_sine={"name":"excA_sine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.excitationBdata_sine={"name":"excB_sine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}

		self.pos1data_cosine={"name":"pos1_cosine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.pos2data_cosine={"name":"pos2_cosine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.pos3data_cosine={"name":"pos3_cosine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.pos4data_cosine={"name":"pos4_cosine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.excitationAdata_cosine={"name":"excA_cosine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
		self.excitationBdata_cosine={"name":"excB_cosine","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[]}
	def writeBufferToFile(self,file,buffer):
		print(buffer["name"])
		group=file.create_group(buffer["name"])
		for data in ["buffer","acqTimestamp","flags","currentSize","flagIndex","bunchedMatrix","bunchIndexes","I","Q","complex","amp","decay","tau","q","qm","SDm","SEm","psi","zeta","phi","bunches"]:
			if data in buffer:
				group.create_dataset(data,data=buffer[data])

	def writeDataToFile(self,fileName):
		h5f = h5py.File(fileName, 'w')
		for buffer in [self.pos1data_sine,self.pos2data_sine,self.pos3data_sine,self.pos4data_sine,self.excitationAdata_sine,self.excitationBdata_sine,self.pos1data_cosine,self.pos2data_cosine,self.pos3data_cosine,self.pos4data_cosine,self.excitationAdata_cosine,self.excitationBdata_cosine]:
			self.writeBufferToFile(h5f,buffer)
		h5f.close()
	def saveData(self):
		name = QtWidgets.QFileDialog.getSaveFileName(self, 'Save data as hdf file',adtFileStorage+"/data_from_master_app/delay_test/")
		name=name[0]
		if name:
			self.writeDataToFile(name)
		else:
			self.addLog("File saving cancelled")

	def addLog(self,msg):
		self.text.appendPlainText(msg)
		self.text.moveCursor(QtGui.QTextCursor.End)
	def run(self):
		#setup subscriptio
		dialog=ADTIntDialog("Harmonic",1)
		self.N=dialog.get()
		self.FTW=self.N*self.frev*(2**32)/self.fS
		self.japc.setParam(self.dspu+'/Excitation',{"Dds1ExcFTW":self.FTW,"Dds1ExcAmplitude":self.amplitude})
		self.addLog("Using harmonic: {}".format(self.N))
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos1,self.subscriptionFuncPos1Exception,dataFilterOverride={'bufferName':0,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos2,self.subscriptionFuncPos2Exception,dataFilterOverride={'bufferName':1,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos3,self.subscriptionFuncPos3Exception,dataFilterOverride={'bufferName':2,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos4,self.subscriptionFuncPos4Exception,dataFilterOverride={'bufferName':2,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncExcitationA,self.subscriptionFuncExcitationAException,dataFilterOverride={'bufferName':4,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncExcitationB,self.subscriptionFuncExcitationBException,dataFilterOverride={'bufferName':5,"decimation":0,"offset":0,"raw":False,"size":0})
		self.checkReadyToRock.connect(self.checkIfReadyToRock)
		self.japc.startSubscriptions()
		self.startButton.clicked.connect(self.runExcitation)
		self.cancelButton.clicked.connect(self.cancel)
		self.show()
		self.exec_()
		self.japc.stopSubscriptions()
		self.japc.clearSubscriptions()
		plt.close('all')
	
	def runExcitation(self):
		if self.state==State.FirstExcitation:
			self.addLog("Starting Excitation")
			self.japc.setParam(self.dspu+'/Command',{"TtuTrigManualExcitation":True})
			self.startButton.setEnabled(False)
		elif self.state==State.SecondExcitation:
			self.addLog("Switching to cosine")
			self.mode=2
			self.japc.setParam(self.dspu+'/GuruVolatileControl',{"test1":self.mode,"test2":self.bunches})
			self.japc.setParam(self.dspu+'/Command',{"TtuTrigManualExcitation":True})
		else:
			self.addLog("Buffers are not ready, chill bruh")
	def cancel(self):
		self.close()

	def bufferToBunchedMatric(self,data):
		flag=data["flagIndex"]
		acqLen=len(data["buffer"])
		K = int(np.floor((acqLen - flag + 1) / self.Nh))
		data = np.reshape(data["buffer"][flag:int((self.Nh*K-1+flag))+1], (int(K),int(self.Nh)))
		data=np.swapaxes(data,0,1)
		return data

	def insertBunch(self,data):
		if self.test:
			acqLen=len(data["buffer"])
			turns=int(np.floor( (acqLen-4100)/self.Nh))
			amplitude=2000.0
			tune=0.31
			offset=2000.0
			damping_time=100.0
			for i in range(turns):
				data["buffer"][i*self.Nh]=np.int16( amplitude*np.exp((-1.0/damping_time)*float(i) )*np.sin(tune*2.0*np.pi*float(i) +self.test_phase)+offset +np.random.normal(0,0.01*amplitude))
				data["buffer"][i*self.Nh+100]=np.int16( amplitude*np.exp((-1.0/damping_time)*float(i) )*np.sin(tune*2.0*np.pi*float(i) +self.test_phase)+offset +np.random.normal(0,0.01*amplitude))
			#self.test_phase+=0.333*np.pi
	def analysis(self):
		self.addLog("starting analysis")
		plt.figure(self.dspu+"measure_delay_using_excitation") # standard observation, raw ADC data
		plt.clf()

		plt.subplot(6,2,1)
		plt.plot(self.pos1data_sine["buffer"][0:10000])
		plt.title('Pos1 sine')

		plt.subplot(6,2,2)
		plt.plot(self.pos1data_cosine["buffer"][0:10000])
		plt.title('Pos1 cosine')
			
		plt.subplot(6,2,3)
		plt.plot(self.pos2data_sine["buffer"][0:10000])
		plt.title('Pos2 sine')

		plt.subplot(6,2,4)
		plt.plot(self.pos2data_cosine["buffer"][0:10000])
		plt.title('Pos2 cosine')

		plt.subplot(6,2,5)
		plt.plot(self.pos3data_sine["buffer"][0:10000])
		plt.title('Pos3 sine')

		plt.subplot(6,2,6)
		plt.plot(self.pos3data_cosine["buffer"][0:10000])
		plt.title('Pos3 cosine')

		plt.subplot(6,2,7)
		plt.plot(self.pos4data_sine["buffer"][0:10000])
		plt.title('Pos4 sine') 

		plt.subplot(6,2,8)
		plt.plot(self.pos4data_cosine["buffer"][0:10000])
		plt.title('Pos4 cosine')

		plt.subplot(6,2,9)
		plt.plot(self.excitationAdata_sine["buffer"][0:10000])
		plt.title('ExcitationA sine')

		plt.subplot(6,2,10)
		plt.plot(self.excitationAdata_cosine["buffer"][0:10000])
		plt.title('ExcitationA cosine')

		plt.subplot(6,2,11)
		plt.plot(self.excitationBdata_sine["buffer"][0:10000])
		plt.title('ExcitationB sine')

		plt.subplot(6,2,12)
		plt.plot(self.excitationBdata_cosine["buffer"][0:10000])
		plt.title('ExcitationB cosine')
		plt.tight_layout()
		plt.show()
		self.addLog("Starting Analysis with plane: "+self.plane)
		if self.plane=="Hor":
			self.kI=np.array([-1.5,-1,5,-1,-1.5])/8.1667
			self.kQ=np.array([1,-11,0,11,-1])/22.35
			self.kappa=-np.mod(2*2*np.pi*0.31-np.pi/2,2*np.pi)
		elif self.plane=="Ver":
			self.kI=np.array([-4 , -6 , 20 , -6 , -4])/30.25
			self.kQ=np.array([1 ,  -4 ,  0  , 4  , -1])/8.807
			self.kappa=-np.mod(2*2*np.pi*0.31-np.pi/2,2*np.pi)
		else:
			self.kI=np.array([-1, -4/3, 14/3, -4/3, -1])/7.108
			self.kQ=np.array([1, -5.5, 0, 5.5, -1])/11.635
			self.kappa=-np.mod(2*2*np.pi*0.3-np.pi/2,2*np.pi)
		try:
			self.N0= np.where(self.excitationAdata_sine["bunchedMatrix"][1] != 0)[0][0]
			first=True
			for buffer in [self.pos1data_sine,self.pos2data_sine,self.pos3data_sine,self.pos4data_sine,self.pos1data_cosine,self.pos2data_cosine,self.pos3data_cosine,self.pos4data_cosine]:
				buffer["I"]=[]
				buffer["Q"]=[]
				buffer["complex"]=[]
				buffer["amp"]=[]
				buffer["decay"]=[]
				buffer["tau"]=[]
				buffer["q"]=[]
				buffer["qm"]=[]
				buffer["SDm"]=[]
				buffer["SEm"]=[]
				buffer["psi"]=[]
				buffer["zeta"]=[]
				buffer["phi"]=[]
				buffer["bunches"]=[]
				buffer["bunchData"]=[]
				for bunch in buffer["bunchIndexes"]:
					buffer["bunchData"].append(buffer["bunchedMatrix"][bunch][self.nt])
					buffer["I"].append(np.convolve(buffer["bunchedMatrix"][bunch][self.nt], self.kI)[:-4])
					buffer["Q"].append(np.convolve(buffer["bunchedMatrix"][bunch][self.nt], self.kQ)[:-4])
					buffer["complex"].append(np.vectorize(complex)(buffer["I"][-1],buffer["Q"][-1]))
					buffer["amp"].append(np.abs(buffer["complex"][-1]))
					buffer["decay"].append(np.log(buffer["amp"][-1]))
					buffer["tau"].append(1.0/np.subtract(buffer["decay"][-1][0:-1],buffer["decay"][-1][1:] ))
					buffer["q"].append((1.0/(2.0*np.pi))*np.diff(np.unwrap(np.angle(buffer["complex"][-1]))) )
					buffer["qm"].append(np.mean(buffer["q"][-1][self.sub]))
					buffer["SDm"].append(np.std(buffer["q"][-1][self.sub]))
					buffer["SEm"].append(buffer["SDm"][-1]/np.sqrt(len(self.sub)))
					temp=np.exp([-1j*2.0*np.pi*(x-1) for  x in self.nt])*buffer["qm"][-1]
					buffer["psi"].append(np.unwrap(np.angle(buffer["complex"][-1]* temp  )-self.kappa))
					buffer["zeta"].append(np.mean(buffer["psi"][-1][self.sub]))
					buffer["phi"].append(np.mod(-np.mean(buffer["zeta"][-1])+2.0*np.pi*np.mean(buffer["qm"][-1]*(4.5-self.N0)),2.0*np.pi))
					if buffer["phi"][-1]>np.pi:
						buffer["phi"][-1]=buffer["phi"][-1]-2.0*np.pi
					if first:
						plt.figure(self.dspu+"measure_delay_using_excitation_one_bunch") # standard observation, raw ADC data
						plt.clf()

						plt.subplot(6,2,1)
						plt.plot(buffer["I"][-1])
						plt.title('I')

						plt.subplot(6,2,2)
						plt.plot(buffer["Q"][-1])
						plt.title('Q')

						plt.subplot(6,2,3)
						plt.plot(buffer["amp"][-1])
						plt.title('amp')

						plt.subplot(6,2,4)
						plt.plot(buffer["decay"][-1])
						plt.title('decay')

						plt.subplot(6,2,5)
						plt.plot(buffer["tau"][-1])
						plt.title('tau')

						plt.subplot(6,2,6)
						plt.plot(buffer["q"][-1])
						plt.title('q')

						plt.subplot(6,2,7)
						plt.plot(buffer["psi"][-1])
						plt.title('psi')

						plt.subplot(6,2,8)
						table=plt.table(cellText=[["qm","{:.4f}".format(buffer["qm"][-1])],["SDm","{:.4f}".format(buffer["SDm"][-1])],["SEm","{:.4f}".format(buffer["SEm"][-1])], ["zeta","{:.4f}".format(buffer["zeta"][-1])],["phi","{:.4f}".format(buffer["phi"][-1])] ])
						table.auto_set_font_size(False)
						table.set_fontsize(14)
						plt.xticks([])

						plt.show()
				buffer["bunchData"]=np.array(buffer["bunchData"])
				buffer["I"]=np.array(buffer["I"])
				buffer["Q"]=np.array(buffer["Q"])
				buffer["complex"]=np.array(buffer["complex"])
				buffer["amp"]=np.array(buffer["amp"])
				buffer["decay"]=np.array(buffer["decay"])
				buffer["tau"]=np.array(buffer["tau"])
				buffer["q"]=np.array(buffer["q"])
				buffer["qm"]=np.array(buffer["qm"])
				buffer["SDm"]=np.array(buffer["SDm"])
				buffer["SEm"]=np.array(buffer["SEm"])
				buffer["psi"]=np.array(buffer["psi"])
				buffer["zeta"]=np.array(buffer["zeta"])
				buffer["phi"]=np.array(buffer["phi"])

				buffer["orbit"]=np.mean(buffer["bunchData"])
				buffer["centeredBunchData"]=buffer["bunchData"]-buffer["orbit"]

			plt.figure(self.dspu+"measure_delay_using_excitation_post_processing")
			plt.clf()
			plt.subplot(321)
			plt.plot(self.nb,self.excitationAdata_sine["bunchedMatrix"][:,self.N0],self.nb,self.excitationBdata_sine["bunchedMatrix"][:,self.N0]);
			plt.grid(True)
			plt.xlabel("Bucket index")
			plt.ylabel("Excitation amplitude/ a.u")
			plt.title('Excitation signal (Mode={}, N={})'.format(self.mode,self.N))
			plt.xlim([self.nb[0],self.nb[-1]])
			
			plt.subplot(323)
			plt.plot(self.pos1data_sine["bunchIndexes"],self.pos1data_sine["qm"],'or',label="Q7 sine")
			plt.plot(self.pos2data_sine["bunchIndexes"],self.pos2data_sine["qm"],'og',label="Q9 sine")
			plt.plot(self.pos3data_sine["bunchIndexes"],self.pos3data_sine["qm"],'ob',label="Q8 sine")
			plt.plot(self.pos4data_sine["bunchIndexes"],self.pos4data_sine["qm"],'om',label="Q10 sine")

			plt.plot(self.pos1data_cosine["bunchIndexes"],self.pos1data_cosine["qm"],'vr',label="Q7 cosine")
			plt.plot(self.pos2data_cosine["bunchIndexes"],self.pos2data_cosine["qm"],'vg',label="Q9 sine")
			plt.plot(self.pos3data_cosine["bunchIndexes"],self.pos3data_cosine["qm"],'vb',label="Q8 sine")
			plt.plot(self.pos4data_cosine["bunchIndexes"],self.pos4data_cosine["qm"],'vm',label="Q10 sine")
			plt.grid(True)
			plt.xlabel("Bunch index")
			plt.ylabel("Tune")
			plt.title('Tune per PU per bunch')
			plt.ylim([0.26,0.33])
			plt.legend(loc='lower left')

			t1=10
			t2=15
			plt.subplot(325)
			if self.pos1data_sine["bunchIndexes"].size>0:
				plt.plot(self.pos1data_sine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos1data_sine["complex"][:,t1])),np.log(np.abs(self.pos1data_sine["complex"][:,t2]))),'or',label="Q7 sine")
				plt.plot(self.pos1data_cosine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos1data_cosine["complex"][:,t1])),np.log(np.abs(self.pos1data_cosine["complex"][:,t2]))),'vr',label="Q7 cosine")
			if self.pos2data_sine["bunchIndexes"].size>0:
				plt.plot(self.pos2data_sine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos2data_sine["complex"][:,t1])),np.log(np.abs(self.pos2data_sine["complex"][:,t2]))),'og',label="Q9 sine")
				plt.plot(self.pos2data_cosine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos2data_cosine["complex"][:,t1])),np.log(np.abs(self.pos2data_cosine["complex"][:,t2]))),'vg',label="Q9 cosine")
			if self.pos3data_sine["bunchIndexes"].size>0:
				plt.plot(self.pos3data_sine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos3data_sine["complex"][:,t1])),np.log(np.abs(self.pos3data_sine["complex"][:,t2]))),'ob',label="Q8 sine")
				plt.plot(self.pos3data_cosine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos3data_cosine["complex"][:,t1])),np.log(np.abs(self.pos3data_cosine["complex"][:,t2]))),'vb',label="Q8 cosine")
			if self.pos4data_sine["bunchIndexes"].size>0:
				plt.plot(self.pos4data_sine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos4data_sine["complex"][:,t1])),np.log(np.abs(self.pos4data_sine["complex"][:,t2]))),'om',label="Q10 sine")
				plt.plot(self.pos4data_cosine["bunchIndexes"], (t2-t1)/np.subtract(np.log(np.abs(self.pos4data_cosine["complex"][:,t1])),np.log(np.abs(self.pos4data_cosine["complex"][:,t2]))),'vm',label="Q10 cosine")
			
			plt.grid(True)
			plt.xlabel("Bunch index")
			plt.ylabel("turns")
			plt.title('Damping time per PU per bunch')
			#plt.ylim([5,25])
			plt.legend(loc='lower left')

			plt.subplot(322)
			#np.tile(self.nt,(self.pos1data_sine["complex"].shape[0],1)).T,
			plt.plot(np.abs(self.pos1data_sine["complex"]).T,'-r',label="Q7 sine")
			plt.plot(np.abs(self.pos2data_sine["complex"]).T,'-g',label="Q9 sine")
			plt.plot(np.abs(self.pos3data_sine["complex"]).T,'-b',label="Q8 sine")
			plt.plot(np.abs(self.pos4data_sine["complex"]).T,'-m',label="Q10 sine")

			plt.plot(np.abs(self.pos1data_cosine["complex"]).T,'-r',label="Q7 cosine")
			plt.plot(np.abs(self.pos2data_cosine["complex"]).T,'-g',label="Q9 cosine")
			plt.plot(np.abs(self.pos3data_cosine["complex"]).T,'-b',label="Q8 cosine")
			plt.plot(np.abs(self.pos4data_cosine["complex"]).T,'-m',label="Q10 cosine")
			plt.grid(True)
			plt.xlabel("turn number")
			plt.ylabel("Amplitude / a.u.")
			plt.title('Oscillation amplitude per PU per turn')
			plt.xlim([9,30])

			plt.subplot(324)
			plt.plot(self.pos1data_sine["q"].T,'-r',label="Q7 sine")
			plt.plot(self.pos2data_sine["q"].T,'-g',label="Q9 sine")
			plt.plot(self.pos3data_sine["q"].T,'-b',label="Q8 sine")
			plt.plot(self.pos4data_sine["q"].T,'-m',label="Q10 sine")

			plt.plot(self.pos1data_cosine["q"].T,'-r',label="Q7 cosine")
			plt.plot(self.pos2data_cosine["q"].T,'-g',label="Q9 cosine")
			plt.plot(self.pos3data_cosine["q"].T,'-b',label="Q8 cosine")
			plt.plot(self.pos4data_cosine["q"].T,'-m',label="Q10 cosine")
			plt.grid(True)
			plt.xlabel("turn number")
			plt.ylabel("Tune")
			plt.title('tune per PU per turn')
			plt.xlim([9,30])

			plt.subplot(326)
			plt.plot(self.pos1data_sine["tau"].T,'-r',label="Q7 sine")
			plt.plot(self.pos2data_sine["tau"].T,'-g',label="Q9 sine")
			plt.plot(self.pos3data_sine["tau"].T,'-b',label="Q8 sine")
			plt.plot(self.pos4data_sine["tau"].T,'-m',label="Q10 sine")

			plt.plot(self.pos1data_cosine["tau"].T,'-r',label="Q7 cosine")
			plt.plot(self.pos2data_cosine["tau"].T,'-g',label="Q9 cosine")
			plt.plot(self.pos3data_cosine["tau"].T,'-b',label="Q8 cosine")
			plt.plot(self.pos4data_cosine["tau"].T,'-m',label="Q10 cosine")
			plt.grid(True)
			plt.xlabel("turn number")
			plt.ylabel("turns")
			plt.title('damping time per PU per bunch')
			plt.xlim([9,30])
			plt.show()

			#I=cosine=real
			self.pos1data_cosine["complex_pickup_data"]=self.pos1data_cosine["centeredBunchData"]+ self.pos1data_sine["centeredBunchData"]*1j
			self.pos2data_cosine["complex_pickup_data"]=self.pos2data_cosine["centeredBunchData"]+ self.pos2data_sine["centeredBunchData"]*1j
			self.pos3data_cosine["complex_pickup_data"]=self.pos3data_cosine["centeredBunchData"]+ self.pos3data_sine["centeredBunchData"]*1j
			self.pos4data_cosine["complex_pickup_data"]=self.pos4data_cosine["centeredBunchData"]+ self.pos4data_sine["centeredBunchData"]*1j
			
			plt.figure('Loop Delay (per PU per turn (Mode={}, N={})'.format(self.mode,self.N))
			plt.clf()
			plt.subplot()
			plt.plot(0,0,'bo',0,0,'ro')
			x = 0.0
			dt = x * 1/8.333
			tau = self.N/3564
			max_r=0
			if self.N>2:

				for k in range(1,35):
					#[bunch][turn]
					#          V what is this - sign doing here
					#P1 = mean(-cplx.x1(turn.x1,:).*exp(-j*2*pi*tau*(idx-1 + dt)));
					#P2 = mean(cplx.x2(turn.x2,:).*exp(-j*2*pi*tau*(idx-1 + dt)));
					if self.pos1data_sine["bunchIndexes"].size>0:
						M1=1.0/np.sqrt(len(self.pos1data_cosine["bunchIndexes"]))
						P1=np.mean(-self.pos1data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos1data_cosine["bunchIndexes"] +dt ) ))
						plt.plot(np.real(P1),np.imag(P1),'ro')
						x_err=M1*np.std(np.real(-self.pos1data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos1data_cosine["bunchIndexes"] +dt ) )))
						y_err=M1*np.std(np.imag(-self.pos1data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos1data_cosine["bunchIndexes"] +dt ) )))
						plt.errorbar(np.real(P1),np.imag(P1),xerr=x_err,yerr=y_err )
						max_r=max(max_r,abs(np.real(P1)))
						max_r=max(max_r,abs(np.imag(P1)))
					if self.pos2data_sine["bunchIndexes"].size>0:	
						M2=1.0/np.sqrt(len(self.pos2data_cosine["bunchIndexes"]))
						P2=np.mean(self.pos2data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos2data_cosine["bunchIndexes"] +dt ) ))
						plt.plot(np.real(P2),np.imag(P2),'go')
						x_err=M2*np.std(np.real(-self.pos2data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos2data_cosine["bunchIndexes"] +dt ) )))
						y_err=M2*np.std(np.imag(-self.pos2data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos2data_cosine["bunchIndexes"] +dt ) )))
						plt.errorbar(np.real(P2),np.imag(P2),xerr=x_err,yerr=y_err )
						max_r=max(max_r,abs(np.real(P2)))
						max_r=max(max_r,abs(np.imag(P2)))
					if self.pos3data_sine["bunchIndexes"].size>0:
						M3=1.0/np.sqrt(len(self.pos2data_cosine["bunchIndexes"]))
						P3=np.mean(-self.pos3data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos3data_cosine["bunchIndexes"] +dt ) ))
						plt.plot(np.real(P3),np.imag(P3),'bo')
						x_err=M3*np.std(np.real(-self.pos3data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos3data_cosine["bunchIndexes"] +dt ) )))
						y_err=M3*np.std(np.imag(-self.pos3data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos3data_cosine["bunchIndexes"] +dt ) )))
						plt.errorbar(np.real(P3),np.imag(P3),xerr=x_err,yerr=y_err )
						max_r=max(max_r,abs(np.real(P3)))
						max_r=max(max_r,abs(np.imag(P3)))
					if self.pos4data_sine["bunchIndexes"].size>0:
						M4=1.0/np.sqrt(len(self.pos2data_cosine["bunchIndexes"]))
						P4=np.mean(self.pos4data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos4data_cosine["bunchIndexes"] +dt ) ))
						plt.plot(np.real(P4),np.imag(P4),'mo')
						x_err=M4*np.std(np.real(self.pos4data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos4data_cosine["bunchIndexes"] +dt ) )))
						y_err=M4*np.std(np.imag(self.pos4data_cosine["complex_pickup_data"][:,k]*np.exp(-2j*np.pi*tau*(self.pos4data_cosine["bunchIndexes"] +dt ) )))
						plt.errorbar(np.real(P4),np.imag(P4),xerr=x_err,yerr=y_err )
						max_r=max(max_r,abs(np.real(P4)))
						max_r=max(max_r,abs(np.imag(P4)))
				start= int(self.Nh/self.N/2)
				steps=np.append(np.floor(np.linspace(-self.Nh/self.N/2,0,5)),np.ceil(np.linspace(0,self.Nh/self.N/2,5)[1:])).astype(int)
				xmin=0
				xmax=0
				ymin=0
				ymax=0

				for t in steps:
					r=max_r-abs(t)*8
					plt.plot([0,r*np.cos(2.0*np.pi*tau*t/8.333)],[0,r*np.sin(2.0*np.pi*tau*t/8.333)],'ko:')
					plt.text(1.05*r*np.cos(2.0*np.pi*tau*t/8.333),1.05*r*np.sin(2.0*np.pi*tau*t/8.333),str(t))
					xmin=min(xmin,r*np.cos(2.0*np.pi*tau*t/8.333))
					xmax=max(xmax,r*np.cos(2.0*np.pi*tau*t/8.333))
					ymin=min(ymin,r*np.sin(2.0*np.pi*tau*t/8.333))
					ymax=max(ymax,r*np.sin(2.0*np.pi*tau*t/8.333))
				
				plt.xlim([1.06*xmin,1.06*xmax])
				plt.ylim([1.06*ymin,1.06*ymax])
			else:
				phi=np.linspace(0,2.0*np.pi,101)
				P1=-self.pos1data_cosine["complex_pickup_data"][:,2]*np.exp(-2j*np.pi*tau*(self.pos1data_cosine["bunchIndexes"] ) )
				plt.plot(np.real(P1),np.imag(P1),'r.')
				
				P2=-self.pos2data_cosine["complex_pickup_data"][:,3]*np.exp(-2j*np.pi*tau*(self.pos2data_cosine["bunchIndexes"] ) )
				plt.plot(np.real(P2),np.imag(P2),'g.')

				P3=-self.pos3data_cosine["complex_pickup_data"][:,4]*np.exp(-2j*np.pi*tau*(self.pos3data_cosine["bunchIndexes"] ) )
				plt.plot(np.real(P3),np.imag(P3),'b.')

				P4=-self.pos4data_cosine["complex_pickup_data"][:,3]*np.exp(-2j*np.pi*tau*(self.pos4data_cosine["bunchIndexes"] ) )
				plt.plot(np.real(P4),np.imag(P4),'m.')

			red_patch = mpatches.Patch(color='red', label='Q7')
			green_patch = mpatches.Patch(color='green', label='Q9')
			blue_patch = mpatches.Patch(color='blue', label='Q8')
			magenta_patch = mpatches.Patch(color='magenta', label='Q10')
			plt.legend(handles=[red_patch,green_patch, blue_patch,magenta_patch])
			#plt.legend(['ro','go','bo','mo'],["Q7",'Q9','Q8','Q10'])

			plt.show()


			self.saveData()
		except Exception as e:
			logging.error("threw exception in analysis: "+traceback.format_exc())
			self.close()


	def checkIfReadyToRock(self):
		if self.pos1Good and self.pos2Good and self.pos3Good and self.pos4Good and self.ExcitationAGood and self.ExcitationBGood:
			if self.state==State.SettingUp:
				self.addLog("Ready to rock")
				self.state=State.FirstExcitation
			elif self.state==State.FirstExcitation:
				self.addLog("Got all data for sine excitation")
				self.state=State.SecondExcitation
				self.runExcitation()
			elif self.state==State.SecondExcitation:
				self.addLog("Got all data for cosine excitation")
				self.analysis()
			self.pos1Good=False
			self.pos2Good=False
			self.pos3Good=False
			self.pos4Good=False
			self.ExcitationAGood=False
			self.ExcitationBGood=False
	def subscriptionFuncPos1(self, parameterName, newValue):
		if self.state==State.SettingUp:
			self.addLog("Ignoring buffer pos1")
		elif self.state==State.FirstExcitation:
			self.addLog("Got pos1 buffer for sine")
			self.pos1data_sine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos1data_sine)
			self.pos1data_sine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos1data_sine["flags"]=newValue["flags"]
			self.pos1data_sine["currentSize"]=newValue["currentSize"]
			self.pos1data_sine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos1data_sine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos1data_sine)
			self.pos1data_sine["bunchIndexes"]=np.where(self.pos1data_sine["bunchedMatrix"][:,0]!=0 )[0]
		elif self.state==State.SecondExcitation:
			self.addLog("Got pos1 buffer for cosine")
			self.pos1data_cosine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos1data_cosine)
			self.pos1data_cosine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos1data_cosine["flags"]=newValue["flags"]
			self.pos1data_cosine["currentSize"]=newValue["currentSize"]
			self.pos1data_cosine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos1data_cosine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos1data_cosine)
			self.pos1data_cosine["bunchIndexes"]=np.where(self.pos1data_cosine["bunchedMatrix"][:,0]!=0 )[0]
		self.pos1Good=True
		self.checkReadyToRock.emit()
		#self.checkIfReadyToRock()
	def subscriptionFuncPos2(self, parameterName, newValue):
		if self.state==State.SettingUp:
			self.addLog("Ignoring buffer pos2")
		elif self.state==State.FirstExcitation:
			self.addLog("Got pos2 buffer for sine")
			self.pos2data_sine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos2data_sine)
			self.pos2data_sine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos2data_sine["flags"]=newValue["flags"]
			self.pos2data_sine["currentSize"]=newValue["currentSize"]
			self.pos2data_sine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos2data_sine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos2data_sine)
			self.pos2data_sine["bunchIndexes"]=np.where(self.pos2data_sine["bunchedMatrix"][:,0]!=0 )[0]
		elif self.state==State.SecondExcitation:
			self.addLog("Got pos2 buffer for cosine")
			self.pos2data_cosine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos2data_cosine)
			self.pos2data_cosine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos2data_cosine["flags"]=newValue["flags"]
			self.pos2data_cosine["currentSize"]=newValue["currentSize"]
			self.pos2data_cosine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos2data_cosine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos2data_cosine)
			self.pos2data_cosine["bunchIndexes"]=np.where(self.pos2data_cosine["bunchedMatrix"][:,0]!=0 )[0]
		self.pos2Good=True
		#self.checkIfReadyToRock()
		self.checkReadyToRock.emit()
	def subscriptionFuncPos3(self, parameterName, newValue):
		if self.state==State.SettingUp:
			self.addLog("Ignoring buffer pos3")
		elif self.state==State.FirstExcitation:
			self.addLog("Got pos3 buffer for sine")
			self.pos3data_sine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos3data_sine)
			self.pos3data_sine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos3data_sine["flags"]=newValue["flags"]
			self.pos3data_sine["currentSize"]=newValue["currentSize"]
			self.pos3data_sine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos3data_sine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos3data_sine)
			self.pos3data_sine["bunchIndexes"]=np.where(self.pos3data_sine["bunchedMatrix"][:,0]!=0 )[0]
		elif self.state==State.SecondExcitation:
			self.addLog("Got pos3 buffer for cosine")
			self.pos3data_cosine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos3data_cosine)
			self.pos3data_cosine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos3data_cosine["flags"]=newValue["flags"]
			self.pos3data_cosine["currentSize"]=newValue["currentSize"]
			self.pos3data_cosine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos3data_cosine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos3data_cosine)
			self.pos3data_cosine["bunchIndexes"]=np.where(self.pos3data_cosine["bunchedMatrix"][:,0]!=0 )[0]
		self.pos3Good=True
		#self.checkIfReadyToRock()
		self.checkReadyToRock.emit()
	def subscriptionFuncPos4(self, parameterName, newValue):
		if self.state==State.SettingUp:
			self.addLog("Ignoring buffer pos4")
		elif self.state==State.FirstExcitation:
			self.addLog("Got pos4 buffer for sine")
			self.pos4data_sine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos4data_sine)
			self.pos4data_sine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos4data_sine["flags"]=newValue["flags"]
			self.pos4data_sine["currentSize"]=newValue["currentSize"]
			self.pos4data_sine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos4data_sine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos4data_sine)
			self.pos4data_sine["bunchIndexes"]=np.where(self.pos4data_sine["bunchedMatrix"][:,0]!=0 )[0]
		elif self.state==State.SecondExcitation:
			self.addLog("Got pos4 buffer for cosine")
			self.pos4data_cosine["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos4data_cosine)
			self.pos4data_cosine["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos4data_cosine["flags"]=newValue["flags"]
			self.pos4data_cosine["currentSize"]=newValue["currentSize"]
			self.pos4data_cosine["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos4data_cosine["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos4data_cosine)
			self.pos4data_cosine["bunchIndexes"]=np.where(self.pos4data_cosine["bunchedMatrix"][:,0]!=0 )[0]
		self.pos4Good=True
		#self.checkIfReadyToRock()
		self.checkReadyToRock.emit()
	def subscriptionFuncExcitationA(self, parameterName, newValue):
		if self.state==State.SettingUp:
			self.addLog("Ignoring buffer excitaitonA")
		elif self.state==State.FirstExcitation:
			self.addLog("Got excitationA buffer for sine")
			self.excitationAdata_sine["buffer"]=newValue["buffer"]
			self.excitationAdata_sine["acqTimestamp"]=newValue["acqTimestamp"]
			self.excitationAdata_sine["flags"]=newValue["flags"]
			self.excitationAdata_sine["currentSize"]=newValue["currentSize"]
			self.excitationAdata_sine["flagIndex"]= np.where(newValue["flags"] >= 2)[0][1]
			self.excitationAdata_sine["bunchedMatrix"]=self.bufferToBunchedMatric(self.excitationAdata_sine)
			flag=self.excitationAdata_sine["flagIndex"]
			acqLen=len(self.excitationAdata_sine["buffer"])
			self.K = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.nt=range(0,self.K)
		elif self.state==State.SecondExcitation:
			self.addLog("Got excitationA buffer for cosine")
			self.excitationAdata_cosine["buffer"]=newValue["buffer"]
			self.excitationAdata_cosine["acqTimestamp"]=newValue["acqTimestamp"]
			self.excitationAdata_cosine["flags"]=newValue["flags"]
			self.excitationAdata_cosine["currentSize"]=newValue["currentSize"]
			self.excitationAdata_cosine["flagIndex"]= np.where(newValue["flags"] >= 2)[0][1]
			self.excitationAdata_cosine["bunchedMatrix"]=self.bufferToBunchedMatric(self.excitationAdata_cosine)
		self.ExcitationAGood=True
		#self.checkIfReadyToRock()
		self.checkReadyToRock.emit()
	def subscriptionFuncExcitationB(self, parameterName, newValue):
		if self.state==State.SettingUp:
			self.addLog("Ignoring buffer excitationB")
		elif self.state==State.FirstExcitation:
			self.addLog("Got excitationB buffer for sine")
			self.excitationBdata_sine["buffer"]=newValue["buffer"]
			self.excitationBdata_sine["acqTimestamp"]=newValue["acqTimestamp"]
			self.excitationBdata_sine["flags"]=newValue["flags"]
			self.excitationBdata_sine["currentSize"]=newValue["currentSize"]
			self.excitationBdata_sine["flagIndex"]= np.where(newValue["flags"] >= 2)[0][1]
			self.excitationBdata_sine["bunchedMatrix"]=self.bufferToBunchedMatric(self.excitationBdata_sine)
		elif self.state==State.SecondExcitation:	
			self.addLog("Got excitationB buffer for cosine")
			self.excitationBdata_cosine["buffer"]=newValue["buffer"]
			self.excitationBdata_cosine["acqTimestamp"]=newValue["acqTimestamp"]
			self.excitationBdata_cosine["flags"]=newValue["flags"]
			self.excitationBdata_cosine["currentSize"]=newValue["currentSize"]
			self.excitationBdata_cosine["flagIndex"]= np.where(newValue["flags"] >= 2)[0][1]
			self.excitationBdata_cosine["bunchedMatrix"]=self.bufferToBunchedMatric(self.excitationBdata_cosine)
		self.ExcitationBGood=True
		#self.checkIfReadyToRock()
		self.checkReadyToRock.emit()
	def subscriptionFuncPos1Exception(self, parameterName, newValue,exception):
		if self.state==State.SettingUp:
			self.addLog("buffer pos1 exception (don't worry all good)")
			self.pos1Good=True
			#self.checkIfReadyToRock()
			self.checkReadyToRock.emit()
	def subscriptionFuncPos2Exception(self, parameterName, newValue,exception):
		if self.state==State.SettingUp:
			self.addLog("buffer pos2 exception (don't worry all good)")
			self.pos2Good=True
			#self.checkIfReadyToRock()
			self.checkReadyToRock.emit()
	def subscriptionFuncPos3Exception(self, parameterName, newValue,exception):
		if self.state==State.SettingUp:
			self.addLog("buffer pos3 exception (don't worry all good)")
			self.pos3Good=True
			#self.checkIfReadyToRock()
			self.checkReadyToRock.emit()
	def subscriptionFuncPos4Exception(self, parameterName, newValue,exception):
		if self.state==State.SettingUp:
			self.addLog("buffer pos4 exception (don't worry all good)")
			self.pos4Good=True
			#self.checkIfReadyToRock()
			self.checkReadyToRock.emit()
	def subscriptionFuncExcitationAException(self, parameterName, newValue,exception):
		if self.state==State.SettingUp:
			self.addLog("buffer excitationA exception (don't worry all good)")
			self.ExcitationAGood=True
			#self.checkIfReadyToRock()
			self.checkReadyToRock.emit()

	def subscriptionFuncExcitationBException(self, parameterName, newValue,exception):
		if self.state==State.SettingUp:
			self.addLog("buffer excitationB exception (don't worry all good)")
			self.ExcitationBGood=True
			#self.checkIfReadyToRock()
			self.checkReadyToRock.emit()

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for dspu in selectedObjects.dspu:
    	excitationMeasurement=ExcitationMeasurement(dspu,japc)
    	excitationMeasurement.run()
    if not selectedObjects.dspu:
        return False
    return True

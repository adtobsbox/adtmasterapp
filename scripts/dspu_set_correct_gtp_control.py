import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for dspu in selectedObjects.dspu:
        for gtp in ["Gtp1Control","Gtp2Control","Gtp3Control","Gtp4Control"]:
            logging.info("Running dspu_set_correct_gtp_control for dspu "+dspu)
            if gtp=="Gtp1Control":
            	#controlbit 0 for new format and tx enable
            	val=bitToInt([0,7,8,9,10,11,12,13,14,15])
            else:
            	#controlbit 0 for new format
            	val=bitToInt([0,8,9,10,11,12,13,14,15])
            logging.info("setting dspu "+dspu+"/GTPControl#"+gtp+" to "+str(val) )
            japc.setParam(dspu+'/GTPControl#'+gtp,val)
    if not selectedObjects.dspu:
        return False
    return True

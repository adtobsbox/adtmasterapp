import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running beampos_set_test_led for beampos "+beampos)
        val=bitToInt([4])
        logging.info("setting beampos "+beampos+"/Control#testControl to "+str(val) )
        japc.setParam(beampos+'/Control#testControl',val)
    if not selectedObjects.beampos:
        return False
    return True

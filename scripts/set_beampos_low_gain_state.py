import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        for gain in ["gainDelta","gainSum"]:
            logging.info("Running set_beampos_in_low_gain_state for beampos "+beampos)
            val=bitToInt([0,1,2,3,4,7])
            logging.info("setting beampos "+beampos+"/RFFrontEnd#"+gain+" to "+str(val) )
            japc.setParam(beampos+'/RFFrontEnd#'+gain,val)
    if not selectedObjects.beampos:
        return False
    return True

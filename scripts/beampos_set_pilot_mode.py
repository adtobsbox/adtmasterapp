import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
from components.adtmessagedialog import ADTMessageDialog

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	dialog=ADTMessageDialog("Are you sure that you want to set it to pilot mode")
	val=dialog.get()
	if not val:
		logging.info("Cancelling beampos_set_pilot_mode from user input")
		return
	for beampos in selectedObjects.beampos:
		logging.info("Running beampos_set_pilot_mode for beampos "+beampos)
		logging.info("setting beampos "+beampos+"/SetPilotSettings" )
		japc.setParam(beampos+'/SetPilotSettings',{})
	if not selectedObjects.beampos:
		return False
	return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.commonfunctions import *
from components.genericPlot import GenericPlot
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
import os

logging.getLogger('matplotlib.font_manager').disabled = True
class Updater(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # Load the dialog's GUI
        DIRNAME = os.path.split(__file__)[0]
        ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/1plot.ui")
        self.pickupData=np.zeros(4)
        self.pickupUpdated=[False]*4
        self.pickupNames=[""]*5
        self.xticks=[0]*5
        self.title=""
        loadUi(ui_file, self)
        self.plane=""
    def updatePickup0(self, parameterName, newValue ):
        self.pickupData[0]=np.average(newValue)
        self.pickupUpdated[0]=True
        self.checkUpdate()
    def updatePickup1(self, parameterName, newValue ):
        self.pickupData[1]=np.average(newValue)
        self.pickupUpdated[1]=True
        self.checkUpdate()
    def updatePickup2(self, parameterName, newValue ):
        self.pickupData[2]=np.average(newValue)
        self.pickupUpdated[2]=True
        self.checkUpdate()
    def updatePickup3(self, parameterName, newValue ):
        self.pickupData[3]=np.average(newValue)
        self.pickupUpdated[3]=True
        self.checkUpdate()
    def checkUpdate(self):
        if all(self.pickupUpdated):
            self.plotPickups()
    def plotPickups(self):
        self.pickupUpdated=[False]*4
        self.plot0.axes.cla()
        self.plot0.axes.plot(self.xticks[:2]+self.xticks[3:],self.pickupData, '-ob',label='1 Sum Ia')
        self.plot0.axes.set_title(self.title)
        self.plot0.axes.grid(True)
        self.plot0.axes.set_xticks(self.xticks)
        self.plot0.axes.set_xticklabels(self.pickupNames)
        self.plot0.axes.set_xlim(-11, 11)
        self.plot0.axes.set_ylim(np.amin([self.pickupData]), np.amax([self.pickupData]))
        self.plot0.axes.set_xlabel('Pickup')
        self.plot0.axes.set_ylabel('bins')
        self.plot0.canvas.draw()        
        logging.info("updated")

    def run(self):
        if self.plane=="B1H":
            self.pickupNames=['Q9', 'Q7', 'IP4', 'Q8', 'Q10']
            self.title='ADT B1 Hor position'
            self.xticks=[-9, -7, 0, 8, 10]
        if self.plane=="B1V":
            self.pickupNames=['Q10', 'Q8', 'IP4', 'Q7', 'Q9']
            self.title='ADT B1 Ver position' 
            self.xticks=[-10, -8, 0, 7, 9]    
        if self.plane=="B2H":
            self.pickupNames=['Q10', 'Q8', 'IP4', 'Q7', 'Q9']
            self.title='ADT B2 Hor position'
            self.xticks=[-10, -8, 0, 7, 9]
        if self.plane=="B2V":
            self.pickupNames=['Q9', 'Q7', 'IP4', 'Q8', 'Q10']
            self.title='ADT B2 Ver position'
            self.xticks=[-9, -7, 0, 8, 10]     

        self.plot0.axes.plot(self.xticks[:2]+self.xticks[3:],self.pickupData, '-ob',label='1 Sum Ia')
        self.plot0.axes.set_title(self.title)
        self.plot0.axes.grid(True)
        self.plot0.axes.set_xticks(self.xticks)
        self.plot0.axes.set_xticklabels(self.pickupNames)
        self.plot0.axes.set_xlim(-11, 11)
        self.plot0.axes.set_ylim(np.amin(self.pickupData), np.amax(self.pickupData))
        self.plot0.axes.set_xlabel('Pickup')
        self.plot0.axes.set_ylabel('bins')
        plt.tight_layout()
        self.show()
        self.exec_()


def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    plane=""
    bunch=0
    for beampos in selectedObjects.beampos:
        if "Hor" in beampos and "B1" in beampos:
            plane="B1H"
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            plane="B1V"
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            plane="B2H"
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            plane="B2V"
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
    if plane=="":
        return False
    updater=Updater()
    updater.plane=plane
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})

    if plane=="B1H":
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q7.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q8.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q9.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q10.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1H.Q9.Obs4k/AcquisitionSubset#data", updater.updatePickup0,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1H.Q7.Obs4k/AcquisitionSubset#data", updater.updatePickup1,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1H.Q8.Obs4k/AcquisitionSubset#data", updater.updatePickup2,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1H.Q10.Obs4k/AcquisitionSubset#data", updater.updatePickup3,dataFilterOverride={'bunches':str(bunch),"turns":""})   
    elif plane=="B1V":
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q7.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q8.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q9.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q10.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1V.Q10.Obs4k/AcquisitionSubset#data", updater.updatePickup0,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1V.Q8.Obs4k/AcquisitionSubset#data", updater.updatePickup1,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1V.Q7.Obs4k/AcquisitionSubset#data", updater.updatePickup2,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B1V.Q9.Obs4k/AcquisitionSubset#data", updater.updatePickup3,dataFilterOverride={'bunches':str(bunch),"turns":""})   
    elif plane=="B2H":
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q7.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q8.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q9.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q10.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2H.Q10.Obs4k/AcquisitionSubset#data", updater.updatePickup0,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2H.Q8.Obs4k/AcquisitionSubset#data", updater.updatePickup1,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2H.Q7.Obs4k/AcquisitionSubset#data", updater.updatePickup2,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2H.Q9.Obs4k/AcquisitionSubset#data", updater.updatePickup3,dataFilterOverride={'bunches':str(bunch),"turns":""})   
    elif plane=="B2V":
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q7.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q8.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q9.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q10.Obs4k/Trigger',{"triggersEnabled":16,"triggerEverySecond":True})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2V.Q9.Obs4k/AcquisitionSubset#data", updater.updatePickup0,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2V.Q7.Obs4k/AcquisitionSubset#data", updater.updatePickup1,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2V.Q8.Obs4k/AcquisitionSubset#data", updater.updatePickup2,dataFilterOverride={'bunches':str(bunch),"turns":""})
        japc.subscribeParam("ObsBox2.LHC.ADT.DEV.B2V.Q10.Obs4k/AcquisitionSubset#data", updater.updatePickup3,dataFilterOverride={'bunches':str(bunch),"turns":""})   

    japc.startSubscriptions()
    updater.run()
    japc.stopSubscriptions()
    japc.clearSubscriptions()
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1H.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B1V.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2H.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q7.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q8.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q9.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    japc.setParam('ObsBox2.LHC.ADT.DEV.B2V.Q10.Obs4k/Trigger',{"triggersEnabled":0,"triggerEverySecond":False})
    return bunches
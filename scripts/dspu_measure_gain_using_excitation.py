import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
from components.adtintdialog import ADTIntDialog
from components.adtFileStorage import adtFileStorage
import logging
from enum import Enum
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog
from PyQt5 import QtGui
from PyQt5.uic import loadUi
from PyQt5.QtCore import QTimer,pyqtSignal
import os
import numpy as np
import traceback
import h5py
import matplotlib.dates as md
import datetime as dt
import time
logging.getLogger('matplotlib.font_manager').disabled = True

class ExcitationMeasurement(QDialog):
	checkReadyToRock = pyqtSignal()
	def __init__(self,dspu:str,japc: pyjapc.PyJapc,parent=None):
		super().__init__(parent)
		DIRNAME = os.path.split(__file__)[0]
		ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/dspu_gain_measurement.ui")
		loadUi(ui_file, self)
		self.dspu=dspu
		self.plane=""
		if "Hor" in self.dspu:
			self.plane="Hor"
		elif "Ver" in self.dspu:
			self.plane="Ver"
		self.japc=japc

		#parameters for excitation
		self.mode=1 # 0=off, 1=pulse,2=cosine,3=sine
		self.N=1 #number of periods per 1 turn excitaiton
		self.frev=11245 #Hz
		self.fS=self.frev*3564*3 # 120MHz clock
		self.FTW=self.N*self.frev*(2**32)/self.fS# frequency tuning word
		self.amplitude=0.9*(2**14)
		self.ExcitationA=1
		self.ExcitationB=0
		self.bunches=3563
		self.Nh=3564
		self.nb=range(0,self.Nh)
		#self.nt=range(0)
		#self.K=0
		self.sub=range(7,15)
		self.time_between_excitation=60
		self.setting_up=True

		#insert one bunch to test
		self.test=False
		self.test_phase=0.0

		#setup observation
		#												Pos1			Pos2				Pos3				Pos4			ExcitationA 	ExcitationB 
		self.japc.setParam(self.dspu+'/BufferSelectOBS',{"obsBufferSel1":0,"obsBufferSel2":1,"obsBufferSel3":2,"obsBufferSel4":3,"obsBufferSel5":24,"obsBufferSel6":26})
		#																ExcTrig
		self.japc.setParam(self.dspu+'/BufferSetting',{"tagEnable":1,"trigSel":2**23},dataFilterOverride={'channelName':0})

		#setup excitation
		self.japc.setParam(self.dspu+'/Excitation',{"Dds1ExcFTW":self.FTW,"Dds1ExcAmplitude":self.amplitude})
		self.japc.setParam(self.dspu+'/GuruVolatileControl',{"test1":self.mode,"test2":self.bunches})
		self.japc.setParam(self.dspu+'/GuruControl',{"ttuControl2":self.ExcitationA*1+2**8+self.ExcitationB*2+2**9,})

		#keep track of state of buffer
		self.pos1Good=False
		self.pos2Good=False
		self.pos3Good=False
		self.pos4Good=False
		self.ExcitationAGood=False
		self.ExcitationBGood=False

		self.q7_damping_times={}
		self.q8_damping_times={}
		self.q9_damping_times={}
		self.q10_damping_times={}


		#data from buffers
		self.pos1data={"color":"r","name":"pos1","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[],"K":0,"nt":range(0)}
		self.pos2data={"color":"g","name":"pos2","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[],"K":0,"nt":range(0)}
		self.pos3data={"color":"b","name":"pos3","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[],"K":0,"nt":range(0)}
		self.pos4data={"color":"m","name":"pos4","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[],"K":0,"nt":range(0)}
		self.excitationAdata={"color":"c","name":"excA_","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[],"K":0,"nt":range(0)}
		self.excitationBdata={"color":"y","name":"excB","buffer":[],"acqTimestamp":0,"flags":[],"currentSize":0,"flagIndex":0,"bunchedMatrix":[],"bunchIndexes":[],"K":0,"nt":range(0)}

		self.t1=10
		self.t2=20
		self.startTurn.setValue(self.t1)
		self.endTurn.setValue(self.t2)
		self.startTurn.valueChanged.connect(self.startTurnChanged)
		self.endTurn.valueChanged.connect(self.endTurnChanged)
		
		self.timer=QTimer()
		self.timer.timeout.connect(self.runExcitation)
		self.plot0.axes.cla()
		self.plot1.axes.cla()
	def startTurnChanged(self):
		self.t1=self.startTurn.value()
	def endTurnChanged(self):
		self.t2=self.endTurn.value()

	def writeBufferToFile(self,file,buffer):
		group=file.create_group(buffer["name"])
		for data in buffer.keys():
			group.create_dataset(data,data=buffer[data])

	def writeDataToFile(self,fileName):
		h5f = h5py.File(fileName, 'w')
		for buffer in [self.pos1data,self.pos2data,self.pos3data,self.pos4data,self.excitationAdata,self.excitationBdata]:
			self.writeBufferToFile(h5f,buffer)
		h5f.close()
	def saveData(self):
		fileName=adtFileStorage+"/data_from_master_app/gain_test/"+time.strftime("%Y%m%d_%Hh%Mm%Ss")+"_gain_test_using_excitation_"+self.dspu+".h5"
		self.addLog("creating file: "+fileName)
		self.writeDataToFile(fileName)

	def addLog(self,msg):
		self.text.appendPlainText(msg)
		self.text.moveCursor(QtGui.QTextCursor.End)
		print(msg)
	def run(self):
		#setup subscriptio
		dialog=ADTIntDialog("Time in seconds between excitations",self.time_between_excitation)
		self.time_between_excitation=dialog.get()
		if self.time_between_excitation<20:
			logging.error("minimum 20 seconds between excitations")
			raise RuntimeError("minimum 20 seconds between excitations")
			self.cancel()
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos1,self.subscriptionFuncPos1Exception,dataFilterOverride={'bufferName':0,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos2,self.subscriptionFuncPos2Exception,dataFilterOverride={'bufferName':1,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos3,self.subscriptionFuncPos3Exception,dataFilterOverride={'bufferName':2,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncPos4,self.subscriptionFuncPos4Exception,dataFilterOverride={'bufferName':2,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncExcitationA,self.subscriptionFuncExcitationAException,dataFilterOverride={'bufferName':4,"decimation":0,"offset":0,"raw":False,"size":0})
		self.japc.subscribeParam(self.dspu+"/BufferAcquisition", self.subscriptionFuncExcitationB,self.subscriptionFuncExcitationBException,dataFilterOverride={'bufferName':5,"decimation":0,"offset":0,"raw":False,"size":0})

		self.checkReadyToRock.connect(self.checkIfReadyToRock)
		self.japc.startSubscriptions()
		self.startButton.clicked.connect(self.startExcitation)
		self.cancelButton.clicked.connect(self.cancel)
		self.show()
		self.exec_()
		self.japc.stopSubscriptions()
		self.japc.clearSubscriptions()
		plt.close('all')
	def startExcitation(self):
		self.timer.start(1000*self.time_between_excitation)
		self.startButton.setEnabled(False)
		self.runExcitation()

	def runExcitation(self):
		if not self.setting_up:
			self.addLog("Starting Excitation")
			self.japc.setParam(self.dspu+'/Command',{"TtuTrigManualExcitation":True})
		else:
			self.addLog("Buffers are not ready, chill bruh")
	def cancel(self):
		self.close()

	def bufferToBunchedMatric(self,data):
		flag=data["flagIndex"]
		acqLen=len(data["buffer"])
		K = int(np.floor((acqLen - flag + 1) / self.Nh))
		data = np.reshape(data["buffer"][flag:int((self.Nh*K-1+flag))+1], (int(K),int(self.Nh)))
		data=np.swapaxes(data,0,1)
		return data

	def insertBunch(self,data):
		if self.test:
			acqLen=len(data["buffer"])
			turns=int(np.floor( (acqLen-4100)/self.Nh))
			amplitude=2000.0
			tune=0.31
			offset=2000.0
			damping_time=100.0
			for i in range(turns):
				data["buffer"][i*self.Nh]=np.int16( amplitude*np.exp((-1.0/damping_time)*float(i) )*np.sin(tune*2.0*np.pi*float(i) +self.test_phase)+offset +np.random.normal(0,0.01*amplitude))
				#data["buffer"][i*self.Nh+100]=np.int16( amplitude*np.exp((-1.0/damping_time)*float(i) )*np.sin(tune*2.0*np.pi*float(i) +self.test_phase)+offset +np.random.normal(0,0.01*amplitude))
			#self.test_phase+=0.333*np.pi
	def analysis(self):
		self.addLog("starting analysis")
		self.addLog("Starting Analysis with plane: "+self.plane)
		if self.plane=="Hor":
			self.kI=np.array([-1.5,-1,5,-1,-1.5])/8.1667
			self.kQ=np.array([1,-11,0,11,-1])/22.35
			self.kappa=-np.mod(2*2*np.pi*0.31-np.pi/2,2*np.pi)
		elif self.plane=="Ver":
			self.kI=np.array([-4 , -6 , 20 , -6 , -4])/30.25
			self.kQ=np.array([1 ,  -4 ,  0  , 4  , -1])/8.807
			self.kappa=-np.mod(2*2*np.pi*0.31-np.pi/2,2*np.pi)
		else:
			self.kI=np.array([-1, -4/3, 14/3, -4/3, -1])/7.108
			self.kQ=np.array([1, -5.5, 0, 5.5, -1])/11.635
			self.kappa=-np.mod(2*2*np.pi*0.3-np.pi/2,2*np.pi)
		try:
			self.plot1.axes.cla()
			for buffer in [self.pos1data,self.pos2data,self.pos3data,self.pos4data]:
				buffer["I"]=[]
				buffer["Q"]=[]
				buffer["complex"]=[]
				buffer["amp"]=[]
				buffer["decay"]=[]
				buffer["tau"]=[]
				buffer["q"]=[]
				buffer["qm"]=[]
				buffer["SDm"]=[]
				buffer["SEm"]=[]
				buffer["psi"]=[]
				buffer["zeta"]=[]
				buffer["phi"]=[]
				buffer["bunches"]=[]
				buffer["bunchData"]=[]
				for bunch in buffer["bunchIndexes"]:
					self.addLog("bunch {} from buffer {}".format(bunch,buffer["name"]))
					buffer["bunchData"].append(buffer["bunchedMatrix"][bunch][buffer["nt"]])
					self.plot1.axes.plot(buffer["bunchData"][-1],buffer["color"]+"-",alpha=0.5)
					self.plot1.axes.plot(self.excitationAdata["bunchedMatrix"][bunch][self.excitationAdata["nt"]],"c-",alpha=0.5)
					self.plot1.axes.plot(self.excitationBdata["bunchedMatrix"][bunch][self.excitationBdata["nt"]],"y-",alpha=0.5)
					
					buffer["I"].append(np.convolve(buffer["bunchedMatrix"][bunch][buffer["nt"]], self.kI )[:-4])
					buffer["Q"].append(np.convolve(buffer["bunchedMatrix"][bunch][buffer["nt"]], self.kQ)[:-4])
					buffer["complex"].append(np.vectorize(complex)(buffer["I"][-1],buffer["Q"][-1]))
					buffer["amp"].append(np.abs(buffer["complex"][-1]))
					buffer["decay"].append(np.log(buffer["amp"][-1]))
					buffer["tau"].append(1.0/np.subtract(buffer["decay"][-1][0:-1],buffer["decay"][-1][1:] ))
					buffer["q"].append((1.0/(2.0*np.pi))*np.diff(np.unwrap(np.angle(buffer["complex"][-1]))) )
					buffer["qm"].append(np.mean(buffer["q"][-1][self.sub]))
					buffer["SDm"].append(np.std(buffer["q"][-1][self.sub]))
					buffer["SEm"].append(buffer["SDm"][-1]/np.sqrt(len(self.sub)))
					temp=np.exp([-1j*2.0*np.pi*(x-1) for  x in buffer["nt"]])*buffer["qm"][-1]
					buffer["psi"].append(np.unwrap(np.angle(buffer["complex"][-1]* temp  )-self.kappa))
					buffer["zeta"].append(np.mean(buffer["psi"][-1][self.sub]))

				buffer["bunchData"]=np.array(buffer["bunchData"])
				buffer["I"]=np.array(buffer["I"])
				buffer["Q"]=np.array(buffer["Q"])
				buffer["complex"]=np.array(buffer["complex"])
				buffer["amp"]=np.array(buffer["amp"])
				buffer["decay"]=np.array(buffer["decay"])
				buffer["tau"]=np.array(buffer["tau"])
				buffer["q"]=np.array(buffer["q"])
				buffer["qm"]=np.array(buffer["qm"])
				buffer["SDm"]=np.array(buffer["SDm"])
				buffer["SEm"]=np.array(buffer["SEm"])
				buffer["psi"]=np.array(buffer["psi"])
				buffer["zeta"]=np.array(buffer["zeta"])
				buffer["phi"]=np.array(buffer["phi"])
				buffer["orbit"]=np.mean(buffer["bunchData"])
				buffer["centeredBunchData"]=buffer["bunchData"]-buffer["orbit"]

			self.saveData()

			now=md.date2num(dt.datetime.fromtimestamp(time.mktime(time.localtime())))

			red_patch = mpatches.Patch(color='red', label='Q7')
			green_patch = mpatches.Patch(color='green', label='Q9')
			blue_patch = mpatches.Patch(color='blue', label='Q8')
			magenta_patch = mpatches.Patch(color='magenta', label='Q10')
			cyan_patch = mpatches.Patch(color='cyan', label='excitationA')
			yellow_patch = mpatches.Patch(color='yellow', label='excitationB')
			self.plot1.axes.legend(handles=[red_patch,green_patch, blue_patch,magenta_patch,cyan_patch,yellow_patch])
			self.plot1.axes.axvline(x=self.t1)
			self.plot1.axes.axvline(x=self.t2)
			self.plot1.axes.set_title("Raw bunch position")
			self.plot1.axes.grid(True)
			self.plot1.axes.set_xlabel('turn')
			self.plot1.axes.set_ylabel('bunch position a.u')
			self.plot1.canvas.draw()
			
			xmin=None
			xmax=None
			ymin=None
			ymax=None
			if self.pos1data["bunchIndexes"].size>0:
				self.q7_damping_times[now]=np.average((self.t2-self.t1)/np.subtract(np.log(np.abs(self.pos1data["complex"][:,self.t1])),np.log(np.abs(self.pos1data["complex"][:,self.t2]))))
				xmin=np.min(list(self.q7_damping_times.keys()))
				xmax=np.max(list(self.q7_damping_times.keys()))
				ymin=np.min(list(self.q7_damping_times.values()))
				ymax=np.max(list(self.q7_damping_times.values()))
			if self.pos2data["bunchIndexes"].size>0:
				self.q9_damping_times[now]=np.average((self.t2-self.t1)/np.subtract(np.log(np.abs(self.pos2data["complex"][:,self.t1])),np.log(np.abs(self.pos2data["complex"][:,self.t2]))))
				if xmin!=None:
					xmin=min(xmin,np.min(list(self.q9_damping_times.keys())))
					xmax=max(xmax,np.max(list(self.q9_damping_times.keys())))
					ymin=min(ymin,np.min(list(self.q9_damping_times.values())))			
					ymax=max(ymax,np.max(list(self.q9_damping_times.values())))
				else:
					xmin=np.min(list(self.q9_damping_times.keys()))
					xmax=np.max(list(self.q9_damping_times.keys()))
					ymin=np.min(list(self.q9_damping_times.values()))
					ymax=np.max(list(self.q9_damping_times.values()))
			
			if self.pos3data["bunchIndexes"].size>0:
				self.q8_damping_times[now]=np.average((self.t2-self.t1)/np.subtract(np.log(np.abs(self.pos3data["complex"][:,self.t1])),np.log(np.abs(self.pos3data["complex"][:,self.t2]))))
				if xmin!=None:	
					xmin=min(xmin,np.min(list(self.q8_damping_times.keys())))
					xmax=max(xmax,np.max(list(self.q8_damping_times.keys())))
					ymin=min(ymin,np.min(list(self.q8_damping_times.values())))
					ymax=max(ymax,np.max(list(self.q8_damping_times.values())))
				else:
					xmin=np.min(list(self.q8_damping_times.keys()))
					xmax=np.max(list(self.q8_damping_times.keys()))
					ymin=np.min(list(self.q8_damping_times.values()))
					ymax=np.max(list(self.q8_damping_times.values()))
			
			if self.pos4data["bunchIndexes"].size>0:
				self.q10_damping_times[now]=np.average((self.t2-self.t1)/np.subtract(np.log(np.abs(self.pos4data["complex"][:,self.t1])),np.log(np.abs(self.pos4data["complex"][:,self.t2]))))			
				if xmin!=None:
					xmin=min(xmin,np.min(list(self.q10_damping_times.keys())))
					xmax=max(xmax,np.max(list(self.q10_damping_times.keys())))
					ymin=min(ymin,np.min(list(self.q10_damping_times.values())))
					ymax=max(ymax,np.max(list(self.q10_damping_times.values())))
				else:
					xmin=np.min(list(self.q10_damping_times.keys()))
					xmax=np.max(list(self.q10_damping_times.keys()))
					ymin=np.min(list(self.q10_damping_times.values()))
					ymax=np.max(list(self.q10_damping_times.values()))
			if xmin==None:
				self.addLog("no bunch found for any pickup")
				return
				
			
			#plot damping time
			self.plot0.axes.cla()
			self.plot0.axes.plot(list(self.q7_damping_times.keys()),list(self.q7_damping_times.values()), '-or',label='Q7')
			self.plot0.axes.plot(list(self.q8_damping_times.keys()),list(self.q8_damping_times.values()), '-og',label='Q8')
			self.plot0.axes.plot(list(self.q9_damping_times.keys()),list(self.q9_damping_times.values()), '-ob',label='Q9')
			self.plot0.axes.plot(list(self.q10_damping_times.keys()),list(self.q10_damping_times.values()), '-om',label='Q10')
			xfmt = md.DateFormatter('%H:%M:%S')
			self.plot0.axes.xaxis.set_major_formatter(xfmt)
			self.plot0.axes.set_title("Damping time over time")
			self.plot0.axes.grid(True)
			self.plot0.axes.set_xlim(xmin, xmax)
			self.plot0.axes.set_ylim(ymin, ymax)
			self.plot0.axes.set_xlabel('time')
			self.plot0.axes.set_ylabel('damping time [turns]')
			self.plot0.axes.legend(loc='lower left')
			self.plot0.canvas.draw()

			#plot raw buffer   
		except Exception as e:
			logging.error("threw exception in analysis: "+traceback.format_exc())
			self.close()


	def checkIfReadyToRock(self):
		if self.pos1Good and self.pos2Good and self.pos3Good and self.pos4Good and self.ExcitationAGood and self.ExcitationBGood:
			if self.setting_up:
				self.addLog("Ready to rock")
				self.setting_up=False
			else:
				self.addLog("Got all data for pulse excitation")
				self.analysis()
			self.pos1Good=False
			self.pos2Good=False
			self.pos3Good=False
			self.pos4Good=False
			self.ExcitationAGood=False
			self.ExcitationBGood=False
	def subscriptionFuncPos1(self, parameterName, newValue):
		if self.setting_up:
			self.addLog("Ignoring buffer pos1")
		else:
			self.addLog("Got pos1 buffer for pulse")
			self.pos1data["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos1data)
			self.pos1data["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos1data["flags"]=newValue["flags"]
			self.pos1data["currentSize"]=newValue["currentSize"]
			self.pos1data["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos1data["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos1data)
			self.pos1data["bunchIndexes"]=np.where(self.pos1data["bunchedMatrix"][:,0]!=0 )[0]

			flag=self.pos1data["flagIndex"]
			acqLen=len(self.pos1data["buffer"])
			self.pos1data["K"] = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.pos1data["nt"]=range(0,self.pos1data["K"])
		self.pos1Good=True
		self.checkReadyToRock.emit()
	def subscriptionFuncPos2(self, parameterName, newValue):
		if self.setting_up:
			self.addLog("Ignoring buffer pos2")
		else:
			self.addLog("Got pos2 buffer ")
			self.pos2data["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos2data)
			self.pos2data["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos2data["flags"]=newValue["flags"]
			self.pos2data["currentSize"]=newValue["currentSize"]
			self.pos2data["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos2data["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos2data)
			self.pos2data["bunchIndexes"]=np.where(self.pos2data["bunchedMatrix"][:,0]!=0 )[0]
			flag=self.pos2data["flagIndex"]
			acqLen=len(self.pos2data["buffer"])
			self.pos2data["K"] = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.pos2data["nt"]=range(0,self.pos2data["K"])
		self.pos2Good=True
		self.checkReadyToRock.emit()
	def subscriptionFuncPos3(self, parameterName, newValue):
		if self.setting_up:
			self.addLog("Ignoring buffer pos3")
		else:
			self.addLog("Got pos3 buffer ")
			self.pos3data["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos3data)
			self.pos3data["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos3data["flags"]=newValue["flags"]
			self.pos3data["currentSize"]=newValue["currentSize"]
			self.pos3data["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos3data["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos3data)
			self.pos3data["bunchIndexes"]=np.where(self.pos3data["bunchedMatrix"][:,0]!=0 )[0]
			flag=self.pos3data["flagIndex"]
			acqLen=len(self.pos3data["buffer"])
			self.pos3data["K"] = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.pos3data["nt"]=range(0,self.pos3data["K"])
		self.pos3Good=True
		self.checkReadyToRock.emit()
	def subscriptionFuncPos4(self, parameterName, newValue):
		if self.setting_up:
			self.addLog("Ignoring buffer pos4")
		else:
			self.addLog("Got pos4 buffer ")
			self.pos4data["buffer"]=newValue["buffer"]
			self.insertBunch(self.pos4data)
			self.pos4data["acqTimestamp"]=newValue["acqTimestamp"]
			self.pos4data["flags"]=newValue["flags"]
			self.pos4data["currentSize"]=newValue["currentSize"]
			self.pos4data["flagIndex"]= np.where(newValue["flags"] == 3)[0][1]
			self.pos4data["bunchedMatrix"]=self.bufferToBunchedMatric(self.pos4data)
			self.pos4data["bunchIndexes"]=np.where(self.pos4data["bunchedMatrix"][:,0]!=0 )[0]
			flag=self.pos4data["flagIndex"]
			acqLen=len(self.pos4data["buffer"])
			self.pos4data["K"] = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.pos4data["nt"]=range(0,self.pos4data["K"])
		self.pos4Good=True
		self.checkReadyToRock.emit()

	def subscriptionFuncExcitationA(self, parameterName, newValue):
		if self.setting_up:
			self.addLog("Ignoring buffer excitationA")
		else:
			self.addLog("Got excitationA buffer ")
			self.excitationAdata["buffer"]=newValue["buffer"]
			self.excitationAdata["acqTimestamp"]=newValue["acqTimestamp"]
			self.excitationAdata["flags"]=newValue["flags"]
			self.excitationAdata["currentSize"]=newValue["currentSize"]
			self.excitationAdata["flagIndex"]= np.where(newValue["flags"] >= 2)[0][1]
			self.excitationAdata["bunchedMatrix"]=self.bufferToBunchedMatric(self.excitationAdata)
			flag=self.excitationAdata["flagIndex"]
			acqLen=len(self.excitationAdata["buffer"])
			self.excitationAdata["K"] = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.excitationAdata["nt"]=range(0,self.excitationAdata["K"])
		self.ExcitationAGood=True
		self.checkReadyToRock.emit()

	def subscriptionFuncExcitationB(self, parameterName, newValue):
		if self.setting_up:
			self.addLog("Ignoring buffer excitationB")
		else:
			self.addLog("Got excitationB buffer ")
			self.excitationBdata["buffer"]=newValue["buffer"]
			self.excitationBdata["acqTimestamp"]=newValue["acqTimestamp"]
			self.excitationBdata["flags"]=newValue["flags"]
			self.excitationBdata["currentSize"]=newValue["currentSize"]
			self.excitationBdata["flagIndex"]= np.where(newValue["flags"] >= 2)[0][1]
			self.excitationBdata["bunchedMatrix"]=self.bufferToBunchedMatric(self.excitationBdata)
			flag=self.excitationBdata["flagIndex"]
			acqLen=len(self.excitationBdata["buffer"])
			self.excitationBdata["K"] = int(np.floor((acqLen - flag + 1) / self.Nh))
			self.excitationBdata["nt"]=range(0,self.excitationBdata["K"])
		self.ExcitationBGood=True
		self.checkReadyToRock.emit()
	
	def subscriptionFuncPos1Exception(self, parameterName, newValue,exception):
		if self.setting_up:
			self.addLog("buffer pos1 exception (don't worry all good)")
			self.pos1Good=True
			self.checkReadyToRock.emit()
	def subscriptionFuncPos2Exception(self, parameterName, newValue,exception):
		if self.setting_up:
			self.addLog("buffer pos2 exception (don't worry all good)")
			self.pos2Good=True
			self.checkReadyToRock.emit()
	def subscriptionFuncPos3Exception(self, parameterName, newValue,exception):
		if self.setting_up:
			self.addLog("buffer pos3 exception (don't worry all good)")
			self.pos3Good=True
			self.checkReadyToRock.emit()
	def subscriptionFuncPos4Exception(self, parameterName, newValue,exception):
		if self.setting_up:
			self.addLog("buffer pos4 exception (don't worry all good)")
			self.pos4Good=True
			self.checkReadyToRock.emit()

	def subscriptionFuncExcitationAException(self, parameterName, newValue,exception):
		if self.setting_up:
			self.addLog("buffer excitationA exception (don't worry all good)")
			self.ExcitationAGood=True
			self.checkReadyToRock.emit()

	def subscriptionFuncExcitationBException(self, parameterName, newValue,exception):
		if self.setting_up:
			self.addLog("buffer excitationB exception (don't worry all good)")
			self.ExcitationBGood=True
			self.checkReadyToRock.emit()
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for dspu in selectedObjects.dspu:
    	excitationMeasurement=ExcitationMeasurement(dspu,japc)
    	excitationMeasurement.run()
    if not selectedObjects.dspu:
        return False
    return True

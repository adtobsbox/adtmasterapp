from components.logic import SelectedObjects,Variables
from components.filedialog import FileDialog
from components.adtmessagedialog import ADTMessageDialog
from PyQt5.QtWidgets import *
import logging
import components.plotlib
import matplotlib.pyplot as plt
import numpy as np
import time


def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects, variables: Variables):
    filename= variables.get("","obsbox_filename")
    if filename:
        dialog=ADTMessageDialog("Use obsbox file defined in table?")
        if not dialog.get():
            filename=""

    if not filename:
        filename = QFileDialog.getOpenFileName(
            None, 'Open File', plotlib.rootpath, "HDF Files (*.h5)")[0]
    if filename != "":
        file = plotlib.PlotFile(filename)
        if not file.open:
            logging.error("File " + filename + " could not be opened")
            return False

        minBunchLen = 3564
        for element in file.datasets:
            minBunchLen = min(element.cols - 1, minBunchLen)

        text, ok = QInputDialog.getText(None, 'Bunches to plot', 'Select the bunches to plot in the form of "1 2 3 532":')
        if not ok:
            logging.error("plot_obsbox_file cancel by user")
            return False
        else:
            bunchesToPlot=[]
            if text:
                bunchesToPlot=[*map(int ,text.split(' '))]
            if len(bunchesToPlot)==0:
                bunchesToPlot = [x for x in range(minBunchLen)]
                logging.info("could not find any bunches to plot, selecting all")
        # Setup plts
        plt.figure(filename)
        plt.clf()
        ax1 = plt.subplot()

        # Plot all datasets
        element=file.datasets[0]
        maxarr = 0
        ylimits = [0, 0]
        ax1.grid(True)
        ax1.set_title("Data captured from " +
                      element.group + " " + element.dataset)
        ax1.set_ylabel("Bunch position [A.U.]")
        ax1.set_xlabel("Turn number")
        # For every bunch in the dataset
        for i in range(element.cols):
            tempdata = None
            # Check if it only contains zeros
            # if element.data[i][0]==0 and element.data[i][int(len(element.data[i])/2)]==0 and element.data[i][-1]==0:
            #  continue
            # if we dont want to plot this bunch
            if i not in bunchesToPlot:
                continue
            if not np.any(element.data[i]):
                logging.info("skipping bunch "+str(i)+" because its data is all zeros")
                continue
            logging.info("Plotting bunch "+str(i))
            # if notch:
            #     tempdata = np.convolve(element.data[i], np.array([1, -1]))[1:-1]
            # else:
            tempdata = element.data[i]
            ax1.plot(range(len(tempdata)), tempdata, label="bunch " + str(i))
            if ylimits[1] == 0:
                ylimits[1] = max(tempdata)
            else:
                ylimits[1] = max(ylimits[1], max(tempdata))
            if ylimits[0] == 0:
                ylimits[0] = min(tempdata)
            else:
                ylimits[0] = min(ylimits[0], min(tempdata))
            maxarr = max(maxarr, len(tempdata))
        ax1.set_xlim([0, maxarr])
        ax1.set_ylim(ylimits)
        #if view:
        plt.show()
        # else:
        #     plt.savefig(file.PathAndFilename)
        # plt.close("all")
        file.close()
        return True

    else:
        logging.info("No file selected, cancelling")
        return False

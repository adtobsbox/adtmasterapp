import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for dspu in selectedObjects.dspu:
        for gtp in ["Gtp1Reset","Gtp2Reset","Gtp3Reset","Gtp4Reset"]:
            logging.info("Running dspu_gtp_reset for dspu "+dspu)
            val=bitToInt([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
            logging.info("setting dspu "+dspu+"/GTPControl#"+gtp+" to "+str(val) )
            japc.setParam(dspu+'/GTPControl#'+gtp,val)
    if not selectedObjects.dspu:
        return False
    return True

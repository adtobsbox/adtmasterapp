import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running set_beampos_threshold for beampos "+beampos)
        val=3000
        logging.info("setting beampos "+beampos+"/BeamPos#threshold to "+str(val) )
        japc.setParam(beampos+'/BeamPos#threshold',val)
    if not selectedObjects.beampos:
        return False
    return True

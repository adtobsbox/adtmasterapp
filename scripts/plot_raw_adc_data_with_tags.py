import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.adtintdialog import ADTIntDialog
from components.commonfunctions import *
logging.getLogger('matplotlib.font_manager').disabled = True
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    for beampos in selectedObjects.beampos:
        bunch=0
        if "Hor" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
        logging.info("Running plot_raw_adc_data_with_tags for beampos "+beampos)
        bufsize = 10692
        offset=25
        raw = False
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256

        buffer1 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer1_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize,'offset':offset})
        buffer2 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer2_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize,'offset':offset})
        buffer3 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer3_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize,'offset':offset})
        buffer4 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer4_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize,'offset':offset})
        buffer5 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer5_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset})
        buffer6 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer6_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize,'offset':offset})
        buffer7 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer7_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize,'offset':offset})
        buffer8 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer8_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize,'offset':offset})
        buffer9 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer9_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize,'offset':offset})
        buffer10 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer10_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize,'offset':offset})
        buffer11 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer11_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize,'offset':offset})
        buffer12 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer12_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize,'offset':offset})

        timeVector = np.arange(0, len(buffer1)/120.24e6, 1/120.24e6)

        # dialog=ADTMessageDialog(" Set xmin and xmax to be centered around a none zero tag" )
        # setX=dialog.get()
        # if setX:
        #     xmin = np.where(buffer1_tag != 0)[0][0] -15
        #     xmax = np.where(buffer1_tag != 0)[0][0] +15
        
        index = np.where(buffer1_tag != 0)[0][0] 
        if bunch!=None:
            index=index+bunch*3
        xmin=index-15
        xmax=index+15
        
        plt.figure(beampos+"_raw_adc_data") # standard observation, raw ADC data
        plt.clf()
        plt.subplot(311)
        plt.plot(buffer1,'-ob',label='1 Sum Ia')
        plt.plot(buffer2,'-or',label='2 Sum Qa')
        plt.plot(buffer3,'-oc',label='3 Sum Ib')
        plt.plot(buffer4,'-om',label='4 Sum Qb')
        plt.plot(buffer1_tag*10000,label='tag')
        plt.plot(buffer2_tag*10000)
        plt.plot(buffer3_tag*10000,label='tag')
        plt.plot(buffer4_tag*10000)
        plt.title(beampos + 'raw ADC data Sum signal')
        plt.grid(True)
        #if setX:
        plt.xlim(xmin, xmax)
        plt.ylim(np.amin([buffer1[xmin:xmax],buffer2[xmin:xmax],buffer3[xmin:xmax],buffer4[xmin:xmax] ]),np.amax([buffer1[xmin:xmax],buffer2[xmin:xmax],buffer3[xmin:xmax],buffer4[xmin:xmax] ]) )
        #plt.ylim(-33000, 33000)
        plt.legend(loc='lower left')
        #plt.xlabel('120 MHz bunch slots')

        plt.subplot(312)
        plt.plot(buffer5,'-ob',label='5 Delta Ia')
        plt.plot((buffer6),'-or',label='6 Delta Ib')
        plt.plot(buffer9,'-oc',label='9 Delta Ic')
        plt.plot((buffer10),'-om',label='10 Delta Id')
        plt.plot(buffer5_tag*10000,label='tag')
        plt.plot(buffer6_tag*10000)
        plt.plot(buffer9_tag*10000,label='tag')
        plt.plot(buffer10_tag*10000)
        plt.grid(True)
        plt.legend(loc='lower left')
        #plt.xlabel('120 MHz bunch slots')
        #if setX:
        plt.xlim(xmin, xmax)
        plt.ylim(np.amin([buffer5[xmin:xmax],buffer6[xmin:xmax],buffer9[xmin:xmax],buffer10[xmin:xmax] ]),np.amax([buffer5[xmin:xmax],buffer6[xmin:xmax],buffer9[xmin:xmax],buffer10[xmin:xmax] ]) )
        #plt.ylim(-33000, 33000)
        plt.title(beampos + ' raw ADC Delta signal I')
        #plt.ylabel('Estimated Q0')

        plt.subplot(313)
        plt.plot(buffer7,'-oc',label='7 Delta Qa')
        plt.plot((buffer8),'-om',label='8 Delta Qb')
        plt.plot(buffer11,'-ob',label='11 Delta Qc')
        plt.plot((buffer12),'-or',label='12 Delta Qd')
        plt.plot(buffer7_tag*10000,label='tag')
        plt.plot(buffer8_tag*10000)
        plt.plot(buffer11_tag*10000,label='tag')
        plt.plot(buffer12_tag*10000)
        plt.grid(True)
        plt.legend(loc='lower left')
        plt.xlabel('120 MHz bunch slots')
        #if setX:
        plt.xlim(xmin, xmax)
        plt.ylim(np.amin([buffer7[xmin:xmax],buffer8[xmin:xmax],buffer11[xmin:xmax],buffer12[xmin:xmax] ]),np.amax([buffer7[xmin:xmax],buffer8[xmin:xmax],buffer11[xmin:xmax],buffer12[xmin:xmax] ]) )
        #plt.ylim(-33000, 33000)
        plt.title(beampos + ' raw ADC Delta signal Q')

        #plt.ylabel('Estimated Q0')
        plt.tight_layout()
        plt.show()
    if not selectedObjects.beampos:
        return False
    return bunches

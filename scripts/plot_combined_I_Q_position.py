
import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.commonfunctions import *
logging.getLogger('matplotlib.font_manager').disabled = True
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    for beampos in selectedObjects.beampos:
        logging.info("Running plot_combined_I_Q_position for beampos "+beampos)
        bunch=0
        if "Hor" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
        bufsize = 16384
        offset=25
        raw = False
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256

        buffer1 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer2 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer3 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer4 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer5 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer5_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset})
        buffer6 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer7 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer8 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer9 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer10 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer11 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer12 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize,'offset':offset}))


        timeVector = np.arange(0, len(buffer1)/120.24e6, 1/120.24e6)

        SumI = np.zeros(len(buffer1)+len(buffer3), dtype=np.int16)
        SumI[0::2] = buffer1-0
        SumI[1::2] = buffer3-0
        SumQ = np.zeros(len(buffer2)+len(buffer4), dtype=np.int16)
        SumQ[0::2] = buffer2-0
        SumQ[1::2] = buffer4-0
        timeVectorSum = np.arange(0, len(SumI)/(2*120.24e6), 1/(2*120.24e6))

        DeltaI = np.zeros(len(buffer5)+len(buffer6)+len(buffer9)+len(buffer10), dtype=np.int16)
        DeltaMarker = np.zeros(len(buffer5)+len(buffer6)+len(buffer9)+len(buffer10), dtype=np.int16)
        DeltaI[0::4] = (buffer5-0)
        DeltaI[1::4] = (buffer6-0)
        DeltaI[2::4] = (buffer9-0)
        DeltaI[3::4] = (buffer10-0)
        DeltaMarker[0::4] = buffer5_tag

        DeltaQ = np.zeros(len(buffer7)+len(buffer8)+len(buffer11)+len(buffer12), dtype=np.int16)
        DeltaQ[0::4] = (buffer7-0)
        DeltaQ[1::4] = (buffer8-0)
        DeltaQ[2::4] = (buffer11-0)
        DeltaQ[3::4] = (buffer12-0)
        timeVectorDelta = np.arange(0, len(DeltaI)/(4*120.24e6), 1/(4*120.24e6))

        xmin = 53.90
        xmax = 54
        index = np.where(buffer5_tag != 0)[0][0]
        if bunch!=None:
            index=index+bunch*3
            xmin=index-15
            xmax=index+15
        xmin=xmin*0.008325195
        xmax=xmax*0.008325195
        plt.figure(beampos+"combined_I_Q_position")
        plt.clf()
        plt.subplot(311)
        plt.plot(timeVectorSum*1e6,SumI,'-x', label='Combined I')
        plt.plot(timeVector*1e6,buffer1-0,'ob',label='I 1')
        plt.plot((timeVector+timeVector[1]/2)*1e6,buffer3-0,'or',label='I 3')
        plt.plot(timeVectorSum*1e6,SumQ,'-x', label='Combined Q')
        plt.plot(timeVector*1e6,buffer2-0,'oc',label='Q 2')
        plt.plot((timeVector+timeVector[1]/2)*1e6,buffer4-0,'om',label='Q 4')

        plt.grid(True)
        plt.xlim(xmin, xmax)
        plt.legend(loc='lower left')
        plt.xlabel('Time (us)')

        plt.subplot(312)
        plt.plot(timeVectorDelta*1e6,DeltaI,'-x', label='Combined')
        plt.plot((timeVector)*1e6,(buffer5-0),'ob',label='I 5')
        plt.plot((timeVector+timeVector[1]/4)*1e6,(buffer6-0),'or',label='I 6')
        plt.plot((timeVector+timeVector[1]/2)*1e6,(buffer9-0),'oc',label='I 9')
        plt.plot((timeVector+timeVector[1]/2+timeVector[1]/4)*1e6,(buffer10-0),'om',label='I 10')
        plt.plot((timeVectorDelta)*1e6,(DeltaMarker*10000),label='tag')

        plt.grid(True)
        plt.xlim(xmin, xmax)
        #plt.ylim(np.amin([buffer6[index-15:index+15],buffer9[index-15:index+15],buffer10[index-15:index+15]]),np.amax([buffer6[index-15:index+15],buffer9[index-15:index+15],buffer10[index-15:index+15]]))
        plt.legend(loc='lower left')
        plt.xlabel('Time (us)')

        plt.subplot(313)
        plt.plot(timeVectorDelta*1e6,DeltaQ,'-x', label='Combined')
        plt.plot((timeVector)*1e6,(buffer7-0),'ob',label='Q 7')
        plt.plot((timeVector+timeVector[1]/4)*1e6,(buffer8-0),'or',label='Q 8')
        plt.plot((timeVector+timeVector[1]/2)*1e6,(buffer11-0),'oc',label='Q 11')
        plt.plot((timeVector+timeVector[1]/2+timeVector[1]/4)*1e6,(buffer12-0),'om',label='Q 12')
        plt.grid(True)
        plt.xlim(xmin, xmax)
        plt.legend(loc='lower left')
        plt.xlabel('Time (us)')

        plt.tight_layout()
        plt.show()
    if not selectedObjects.beampos:
        return False
    return bunches

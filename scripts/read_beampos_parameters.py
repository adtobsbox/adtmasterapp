
import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.commonfunctions import *
from numpy import pi as pi
logging.getLogger('matplotlib.font_manager').disabled = True
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    parameters=[]
    for beampos in selectedObjects.beampos:
        plane=""
        pickup=""
        bunch=0
        if "Hor" in beampos and "B1" in beampos:
            plane="B1H"
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            plane="B1V"
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            plane="B2H"
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            plane="B2V"
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
        if "Q7" in beampos:
            pickup="Q7"
        if "Q8" in beampos:
            pickup="Q8"
        if "Q9" in beampos:
            pickup="Q9"
        if "Q10" in beampos:
            pickup="Q10"
        if plane=="" or pickup=="":
            logging.error("plane or pickup not found in beampos name")
            return False
        bpm_position=0
        bpm_set=False
        if plane=="B1H":
            if pickup=="Q7":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1LA/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7L4.B1')[0][0]]
                bpm_set=True
            if pickup=="Q8":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1RB/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8R4.B1')[0][0]]
                bpm_set=True
            if pickup=="Q9":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1LA/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9L4.B1')[0][0]]
                bpm_set=True
            if pickup=="Q10":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1RB/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10R4.B1')[0][0]]
                bpm_set=True
        if plane=="B1V":
            if pickup=="Q7":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1RA/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7R4.B1')[0][0]]
                bpm_set=True
            if pickup=="Q8":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1LB/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8L4.B1')[0][0]]
                bpm_set=True
            if pickup=="Q9":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1RA/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9R4.B1')[0][0]]
                bpm_set=True
            if pickup=="Q10":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1LB/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10L4.B1')[0][0]]
                bpm_set=True

        if plane=="B2H":
            if pickup=="Q7":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2RA/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7R4.B2')[0][0]]
                bpm_set=True
            if pickup=="Q8":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2LB/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8L4.B2')[0][0]]
                bpm_set=True
            if pickup=="Q9":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2RA/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9R4.B2')[0][0]]
                bpm_set=True
            if pickup=="Q10":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2LB/Acquisition')
                bpm_position = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10L4.B2')[0][0]]
                bpm_set=True
        if plane=="B2V":
            if pickup=="Q7":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2LA/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7L4.B2')[0][0]]
                bpm_set=True
            if pickup=="Q8":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2RB/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8R4.B2')[0][0]]
                bpm_set=True
            if pickup=="Q9":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2LA/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9L4.B2')[0][0]]
                bpm_set=True
            if pickup=="Q10":
                bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2RB/Acquisition')
                bpm_position = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10R4.B2')[0][0]]
                bpm_set=True
        if not bpm_set:
            logging.error("BPM data was not fetched")
            return False


        logging.info("Running read_beampos_parameters for beampos "+beampos)
        bufsize = 10692
        offset=25
        raw = False
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256

        buffer13 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer13_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset})
        buffer14 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+13,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer15 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+14,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer16 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+15,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer17 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer17_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize,'offset':offset})
        index = np.where(buffer13_tag == 3)[0][0]
        if bunch!=None:
            index=index+bunch*3
        AngleSum = np.arctan2(buffer14[index],buffer13[index])*180/pi
        MagSum = np.sqrt(buffer14[index]**2 + buffer13[index]**2)
        AngleDelta = np.arctan2(buffer16[index],buffer15[index])*180/pi
        MagDelta = np.sqrt(buffer16[index]**2 + buffer15[index]**2)
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_AngleSum",AngleSum))
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_AngleDelta",AngleDelta))
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_MagSum",MagSum))
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_MagDelta",MagDelta))

        parameters.append((beampos,"parameters_"+str(bpm_position)+"_SumIFinal",buffer13[index]))
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_SumQFinal",buffer14[index]))
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_DeltaIFinal",buffer15[index]))
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_DeltaQFinal",buffer16[index]))

        index = np.where(buffer17_tag == 3)[0][0]
        if bunch!=None:
            index=index+bunch*3
        parameters.append((beampos,"parameters_"+str(bpm_position)+"_PositionFinal",buffer17[index]))

    if not selectedObjects.beampos:
        return False
    return bunches+parameters

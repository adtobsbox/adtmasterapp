import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging
from components.adtintdialog import ADTIntDialog

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    if len(selectedObjects.beampos)>1:
        logging.error("set_beampos_fine_delay should only be run one one BeamPos")
        return False

    for beampos in selectedObjects.beampos:
        logging.info("Running set_beampos_fine_delay for beampos "+beampos)
        old_value=japc.getParam(beampos+'/Clk#delay1')
        dialog=ADTIntDialog("Set fine delay1",old_value)
        delay=dialog.get()
        if delay==None:
            logging.info("Operation cancelled by user")
            return False
        if delay<0 or delay>1024:
            logging.error("fine delay not in range [0,1024]")
            return False

        logging.info("setting beampos "+beampos+"/Clk#delay1 to "+str(delay) )
        japc.setParam(beampos+'/Clk#delay1',delay)

        old_value=japc.getParam(beampos+'/Clk#delay2')
        dialog=ADTIntDialog("Set fine delay2",old_value)
        delay=dialog.get()
        if delay==None:
            logging.info("Operation cancelled by user")
            return False
        if delay<0 or delay>1024:
            logging.error("fine delay not in range [0,1024]")
            return False

        logging.info("setting beampos "+beampos+"/Clk#delay2 to "+str(delay) )
        japc.setParam(beampos+'/Clk#delay2',delay)

    if not selectedObjects.beampos:
        return False
    return True

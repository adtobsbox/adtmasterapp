import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
from components.adtintdialog import ADTIntDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.commonfunctions import *
from components.genericPlot import GenericPlot
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
import os
logging.getLogger('matplotlib.font_manager').disabled = True

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    dialog=ADTIntDialog("Start",0)
    start=dialog.get()
    dialog=ADTIntDialog("Stop",360)
    stop=dialog.get()+1
    dialog=ADTIntDialog("Step",36)
    step=dialog.get()
    if abs(start)>360:
        logging.error("start greater than 360")
        return False
    if abs(stop)>360:
        logging.error("stop greater than 360")
        return False
    
    if len(range(start,stop,step))>20:
        logging.info("maximum 20 steps is allowed")
        return False
    for beampos in selectedObjects.beampos:
        
        bunch=0
        if "Hor" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
        logging.info("Running beampos_test_rotation_angle_vs_position for beampos "+beampos+" and bunch "+str(bunch))
        x=np.arange(start,stop,step,dtype=float)
        x*=np.pi
        x/=180
        y=[]
        sumDeltaDiff=[]
        for i in range(start,stop,step):
            japc.setParam(beampos+'/RFFrontEnd#rotationAngle',i)
            time.sleep(1)
            bufsize = 10692
            offset=25
            raw = False
            chanName = 1
            japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
            japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
            time.sleep(2)
            bufid = 256
            buffer13 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer13_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset})
            buffer14 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+13,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer15 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+14,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer16 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+15,'raw': raw, 'size':bufsize,'offset':offset}))

            buffer17 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer17_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize,'offset':offset})
            index = np.where(buffer17_tag == 3)[0][0]
            if bunch!=None:
                index=index+bunch*3
            position=buffer17[index]
            y.append(position)
            index = np.where(buffer13_tag == 3)[0][0]
            if bunch!=None:
                index=index+bunch*3

            AngleSum = np.arctan2(buffer14[index],buffer13[index])
            AngleDelta = np.arctan2(buffer16[index],buffer15[index])
            vector1=[np.cos(AngleSum),np.sin(AngleSum)]
            vector2=[np.cos(AngleDelta),np.sin(AngleDelta)]
            dot_product = np.dot(vector1, vector2)
            angle = np.arccos(dot_product)
            sumDeltaDiff.append(angle*180.0/np.pi)

        #plt.figure(beampos+"rotationAngle_vs_position")
        #plt.clf()
        fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
        ax.plot(x, y)
        ax.set_rmax(max(y))
        ax.set_title("Position VS rotationAngle", va='bottom')
        index_max = max(range(len(y)), key=y.__getitem__)
        ax.text(x[index_max],max(y), str(max(y))+' @ '+str(x[index_max]*180/np.pi),ha='center',va='center')
        ax.text(3.925,max(y), "sumDeltaAngleDiff @ maxPosition : "+str(sumDeltaDiff[index_max]) ,ha='center',va='center')
        plt.show()
    if not selectedObjects.beampos:
        return False
    return bunches
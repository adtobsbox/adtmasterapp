import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
from components.adtSelectDialog import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    planes=["HB1","VB1","HB2","VB2"]
    selectdialog=adtSelectDialog("Select plane",planes)
    val= selectdialog.get() 
    if not val:
        return False
    success=True
    for plane in planes:
        if plane not in val.keys():
            success=False
    if not success:
        return False
    counter=0
    for plane in planes:
        if val[plane]:
            counter+=1
    if counter!=1:
        return False
    HB1=val["HB1"]
    VB1=val["VB1"]
    HB2=val["HB2"]
    VB2=val["VB2"]
    rfOrHOM=["RF","HOM"]
    selectdialog=adtSelectDialog("Select RF or HOM",rfOrHOM)
    val= selectdialog.get()
    
    success=True
    for sel in rfOrHOM:
        if sel not in val.keys():
            success=False
    if not success:
        return False
    counter=0
    for sel in rfOrHOM:
        if val[sel]:
            counter+=1
    if counter!=1:
        return False
    RF=val["RF"]
    HOM=val["HOM"]

    port1=""
    port2=""
    if HB1 and RF:
        port1="COMtoPort8"
        port2="COMtoPort7"
    elif HB1 and HOM:
        port1="COMtoPort7"
        port2="COMtoPort8"
    elif VB1 and RF:
        port1="COMtoPort6"
        port2="COMtoPort5"
    elif VB1 and HOM:
        port1="COMtoPort5"
        port2="COMtoPort6"
    elif HB2 and RF:
        port1="COMtoPort4"
        port2="COMtoPort3"
    elif HB2 and HOM:
        port1="COMtoPort3"
        port2="COMtoPort4"
    elif VB2 and RF:
        port1="COMtoPort2"
        port2="COMtoPort1"
    elif VB2 and HOM:
        port1="COMtoPort1"
        port2="COMtoPort2"
    else:
        return False
    japc.setParam("ALLRFMux.UX45.ADT1-1/SwitchCfg#switchState", port1)
    japc.setParam("ALLRFMux.UX45.ADT1-2/SwitchCfg#switchState", port1)
    japc.setParam("ALLRFMux.UX45.ADT1-3/SwitchCfg#switchState", port1)
    japc.setParam("ALLRFMux.UX45.ADT1-4/SwitchCfg#switchState", port1)
    japc.setParam("ALLRFMux.UX45.ADT2-1/SwitchCfg#switchState", port2)
    japc.setParam("ALLRFMux.UX45.ADT2-2/SwitchCfg#switchState", port2)
    japc.setParam("ALLRFMux.UX45.ADT2-3/SwitchCfg#switchState", port2)
    japc.setParam("ALLRFMux.UX45.ADT2-4/SwitchCfg#switchState", port2)

    return True

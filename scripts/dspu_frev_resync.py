import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	for dspu in selectedObjects.dspu:
		logging.info("Running dspu_frev_resync for dspu "+dspu)
		val=bitToInt([9])
		logging.info("setting dspu "+dspu+"/GuruVolatileControl#ttuTrigControl to "+str(val) )
		japc.setParam(dspu+'/GuruVolatileControl#ttuTrigControl',val)
	if not selectedObjects.dspu:
		return False
	return True

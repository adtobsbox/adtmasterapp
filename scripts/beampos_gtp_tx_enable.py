import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        for gtp in ["gtp1_control","gtp2_control","gtp3_control","gtp4_control"]:
            logging.info("Running beampos_gtp_tx_enable for beampos "+beampos)
            val=bitToInt([7,15])
            logging.info("setting beampos "+beampos+"/Gtp#"+gtp+" to "+str(val) )
            japc.setParam(beampos+'/Gtp#'+gtp,val)
    if not selectedObjects.beampos:
        return False
    return True
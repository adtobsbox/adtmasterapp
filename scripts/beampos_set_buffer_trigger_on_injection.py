import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
from components.commonfunctions import *

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running beampos_set_buffer_trigger_on_injection for beampos "+beampos)
        #obstimetrig
        val=bitToInt([16])
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#trigSel',val,dataFilterOverride={'channelName': chanName})
        #japc.setParam(beampos+'/Ttu#obsTime',9)
        #beamin
        #val=bitToInt([1])
        #japc.setParam(beampos+'/Ttu#trigSelBeamIn',val)
        
    if not selectedObjects.beampos:
        return False
    return True

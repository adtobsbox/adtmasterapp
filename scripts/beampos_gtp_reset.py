import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        for gtp in ["gtp1_reset","gtp2_reset","gtp3_reset","gtp4_reset"]:
            logging.info("Running beampos_gtp_tx_reset for beampos "+beampos)
            val=bitToInt([0,1,2,8,9,10])
            logging.info("setting beampos "+beampos+"/Gtp#"+gtp+" to "+str(val) )
            japc.setParam(beampos+'/Gtp#'+gtp,val)
    if not selectedObjects.beampos:
        return False
    return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for dspu in selectedObjects.dspu:
        for gtp in ["Gtp1Reset","Gtp2Reset","Gtp3Reset","Gtp4Reset"]:
            logging.info("Running dspu_reset_rx for dspu "+dspu)
            val=bitToInt([6,14])
            logging.info("setting dspu "+dspu+"/GTPControl#"+gtp+" to "+str(val) )
            japc.setParam(dspu+'/GTPControl#'+gtp,val)
    if not selectedObjects.dspu:
        return False
    return True

import pyjapc
from logic import SelectedObjects, Variables
from filedialog import FileDialog
from PyQt5.QtWidgets import *
import logging
import plotlib
import matplotlib.pyplot as plt
import numpy as np
import time


def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects, variables: Variables):
    filename = QFileDialog.getOpenFileName(
        None, 'Open File', plotlib.rootpath, "HDF Files (*.h5)")
    if filename[0] != "":
        filename = filename[0]

        return [("","obsbox_filename",filename)]

    else:
        logging.info("No file selected, cancelling")
        return False

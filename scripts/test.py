import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging
from enum import Enum
import matplotlib.pyplot as plt
import PyQt5
from PyQt5.QtWidgets import QDialog
from PyQt5 import QtGui
from PyQt5.uic import loadUi
import os
from components.adtPlotDialog import ADTPlotDialog

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	plotDialog=ADTPlotDialog(None,6,2)
	plotDialog.run()
import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
logging.getLogger('matplotlib.font_manager').disabled = True
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running plot_adc_sum_delta_vectors for beampos "+beampos)
        bufsize = 10692
        offset=25
        raw = False
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256

        buffer1 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer2 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer3 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer4 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer5 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer6 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer7 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer8 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer9 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer10 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer11 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer12 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize,'offset':offset}))


        plt.figure(beampos+"raw_adc_sum_delta_vectors") # modified observation, raw ADC data for Sum, then processed Sum-Delta-Position data
        plt.clf()
        plt.plot(buffer1,buffer2,'xb',label='1 Sum a')
        plt.plot(buffer3,buffer4,'xr',label='2 Sum a')
        plt.plot(buffer5,buffer7,'o',label='5 Delta a')
        plt.plot(buffer6,buffer8,'o',label='5 Delta b')
        plt.plot(buffer9,buffer11,'o',label='5 Delta c')
        plt.plot(buffer10,buffer12,'o',label='5 Delta d')
        plt.plot(32768 * np.cos(np.arange(-180,181,5)/180*np.pi), 32768 * np.sin(np.arange(-180,181,5)/180*np.pi),'--r',label='ADC full scale')
        plt.xlim(-33000, 33000)
        plt.ylim(-33000, 33000)
        plt.xlabel('I')
        plt.ylabel('Q')
        plt.title(beampos + ' Sum-Delta vectors')
        plt.legend(loc='lower left')
        plt.grid(True)
        plt.tight_layout()
        plt.show()
    if not selectedObjects.beampos:
        return False
    return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging
from components.adtintdialog import ADTIntDialog

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    if len(selectedObjects.beampos)>1:
        logging.error("set_beampos_coarse_delay should only be run one one BeamPos")
        return False


    for beampos in selectedObjects.beampos:
        logging.info("Running set_beampos_coarse_delay for beampos "+beampos)

        old_value=japc.getParam(beampos+'/Clk#tagDeltaIdx')
        dialog=ADTIntDialog("Set coarse delay for delta",old_value)
        delay=dialog.get()
        if delay==None:
            logging.info("Operation cancelled by user")
            return False
        # if delay<0 or delay>1024:
        #     logging.error("fine delay not in range [0,1024]")
        #     return False

        logging.info("setting beampos "+beampos+"/Clk#tagDeltaIdx to "+str(delay) )
        japc.setParam(beampos+'/Clk#tagDeltaIdx',delay)

        old_value=japc.getParam(beampos+'/Clk#tagSumIdx')
        dialog=ADTIntDialog("Set coarse delay for sum",old_value)
        delay=dialog.get()
        if delay=None:
            logging.info("Operation cancelled by user")
            return False
        # if delay<0 or delay>1024:
        #     logging.error("fine delay not in range [0,1024]")
        #     return False

        logging.info("setting beampos "+beampos+"/Clk#tagSumIdx to "+str(delay) )
        japc.setParam(beampos+'/Clk#tagSumIdx',delay)

    if not selectedObjects.beampos:
        return False
    return True

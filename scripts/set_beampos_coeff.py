import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running set_beampos_coeff for beampos "+beampos)
        val=[0.16666158,0.16666158,0.16666158,0.16666158,0.16666158,0.16666158]
        logging.info("setting beampos "+"/BeamPos#sum_rCoeffArray to "+str(val) )
        japc.setParam(beampos+'/BeamPos#sum_rCoeffArray',val)
        logging.info("setting beampos "+"/BeamPos#sum_uCoeffArray to "+str(val) )
        japc.setParam(beampos+'/BeamPos#sum_uCoeffArray',val)
        val=[0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553,0.08331553]
        logging.info("setting beampos "+"/BeamPos#delta_rCoeffArray to "+str(val) )
        japc.setParam(beampos+'/BeamPos#delta_rCoeffArray',val)
        logging.info("setting beampos "+"/BeamPos#delta_uCoeffArray to "+str(val) )
        japc.setParam(beampos+'/BeamPos#delta_uCoeffArray',val)
    if not selectedObjects.beampos:
        return False
    return True

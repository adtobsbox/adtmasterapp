import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
import matplotlib.pyplot as plt
import numpy as np

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    planes=[]
    for beampos in selectedObjects.beampos:
        if "Hor" in beampos and "B1" in beampos:
            planes.append("B1H")
        if "Ver" in beampos and "B1" in beampos:
            planes.append("B1V")
        if "Hor" in beampos and "B2" in beampos:
            planes.append("B2H")
        if "Ver" in beampos and "B2" in beampos:
            planes.append("B2V")
    for dspu in selectedObjects.dspu:
        if "Hor" in dspu and "B1" in dspu:
            planes.append("B1H")
        if "Ver" in dspu and "B1" in dspu:
            planes.append("B1V")
        if "Hor" in dspu and "B2" in dspu:
            planes.append("B2H")
        if "Ver" in dspu and "B2" in dspu:
            planes.append("B2V")
    if not planes:
        logging.error("No planes selected in plot_bunch_position_from_bpm, select a beampos or dspu associated with a plane")
        return False
    if "B1H" in planes:
        logging.info("Running plot_bunch_position_from_bpm plane B1H ")
        # B1 hor Q7 and Q9 is in LA crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1LA/Acquisition')
        position_B1_Hor_Q7 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7L4.B1')[0][0]]
        position_B1_Hor_Q9 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9L4.B1')[0][0]]
        # B1 hor Q8 and Q10 is in RB crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1RB/Acquisition')
        position_B1_Hor_Q8 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8R4.B1')[0][0]]
        position_B1_Hor_Q10 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10R4.B1')[0][0]]

        plt.figure("B1H_BUNCH_POSITION") # standard observation, raw ADC data
        plt.clf()
        plt.plot([-9, -7, 8, 10],[position_B1_Hor_Q9, position_B1_Hor_Q7, position_B1_Hor_Q8, position_B1_Hor_Q10], '-ob',label='1 Sum Ia')
        plt.title('B1 Hor position')
        plt.grid(True)
        plt.xticks([-9, -7, 0, 8, 10], ['Q9', 'Q7', 'IP4', 'Q8', 'Q10'])
        plt.xlim(-11, 11)
        plt.ylim(-2, 2)
        plt.xlabel('Pickup')
        plt.ylabel('mm')
        plt.tight_layout()
        plt.show()
    if "B1V" in planes:
        logging.info("Running plot_bunch_position_from_bpm plane B1V ")
        # B1 ver Q7 and Q9 is in RA crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1RA/Acquisition')
        position_B1_Ver_Q7 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7R4.B1')[0][0]]
        position_B1_Ver_Q9 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9R4.B1')[0][0]]
        # B1 ver Q8 and Q10 is in LB crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B1LB/Acquisition')
        position_B1_Ver_Q8 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8L4.B1')[0][0]]
        position_B1_Ver_Q10 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10L4.B1')[0][0]]

        plt.figure("B1V_BUNCH_POSITION") # standard observation, raw ADC data
        plt.clf()
        plt.plot([-10, -8, 7, 9],[position_B1_Ver_Q10, position_B1_Ver_Q8, position_B1_Ver_Q7, position_B1_Ver_Q9], '-ob',label='1 Sum Ia')
        plt.title('B1 Ver position')
        plt.grid(True)
        plt.xticks([-10, -8, 0, 7, 9], ['Q10', 'Q8', 'IP4', 'Q7', 'Q9'])
        plt.xlim(-11, 11)
        plt.ylim(-2, 2)
        plt.xlabel('Pickup')
        plt.ylabel('mm')
        plt.tight_layout()
        plt.show()
    if "B2H" in planes:
        logging.info("Running plot_bunch_position_from_bpm plane B2H ")
        # B2 hor Q7 and Q9 is in RA crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2RA/Acquisition')
        position_B2_Hor_Q7 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7R4.B2')[0][0]]
        position_B2_Hor_Q9 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9R4.B2')[0][0]]
        # B2 hor Q8 and Q10 is in LB crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2LB/Acquisition')
        position_B2_Hor_Q8 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8L4.B2')[0][0]]
        position_B2_Hor_Q10 = bpmAcquisition['horFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10L4.B2')[0][0]]

        plt.figure("B2H_BUNCH_POSITION") # standard observation, raw ADC data
        plt.clf()
        plt.plot([-10, -8, 7, 9],[position_B2_Hor_Q10, position_B2_Hor_Q8, position_B2_Hor_Q7, position_B2_Hor_Q9], '-or',label='1 Sum Ia')
        plt.title('B2 Hor position')
        plt.grid(True)
        plt.xticks([-10, -8, 0, 7, 9], ['Q10', 'Q8', 'IP4', 'Q7', 'Q9'])
        plt.xlim(-11, 11)
        plt.ylim(-2, 2)
        plt.xlabel('Pickup')
        plt.ylabel('mm')
        plt.tight_layout()
        plt.show()
    if "B2V" in planes:
        logging.info("Running plot_bunch_position_from_bpm plane B2V ")
        # B2 ver Q7 and Q9 is in LA crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2LA/Acquisition')
        position_B2_Ver_Q7 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.7L4.B2')[0][0]]
        position_B2_Ver_Q9 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.9L4.B2')[0][0]]
        # B2 ver Q8 and Q10 is in RB crate
        bpmAcquisition = japc.getParam('LHC.BPM.SX4.B2RB/Acquisition')
        position_B2_Ver_Q8 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.8R4.B2')[0][0]]
        position_B2_Ver_Q10 = bpmAcquisition['verFiltOrbitPos'][np.where(bpmAcquisition['bpmNames'] == 'BPM.10R4.B2')[0][0]]

        plt.figure("B2V_BUNCH_POSITION") # standard observation, raw ADC data
        plt.clf()
        plt.plot([-9, -7, 8, 10],[position_B2_Ver_Q9, position_B2_Ver_Q7, position_B2_Ver_Q8, position_B2_Ver_Q10], '-or',label='1 Sum Ia')
        plt.title('B2 Ver position')
        plt.grid(True)
        plt.xticks([-9, -7, 0, 8, 10], ['Q9', 'Q7', 'IP4', 'Q8', 'Q10'])
        plt.xlim(-11, 11)
        plt.ylim(-2, 2)
        plt.xlabel('Pickup')
        plt.ylabel('mm')
        plt.tight_layout()
        plt.show()
    return True

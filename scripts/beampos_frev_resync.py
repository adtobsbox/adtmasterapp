import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
from components.commonfunctions import *

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running frev_resync for beampos "+beampos)
        val=bitToInt([5])
        logging.info("setting beampos "+beampos+"/Ttu#trigControl to "+str(val) )
        japc.setParam(beampos+'/Ttu#trigControl',val)
    if not selectedObjects.beampos:
        return False
    return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	for dspu in selectedObjects.dspu:
		logging.info("Running dspu_clear_faults_and_overflows for dspu "+dspu)
		val=bitToInt([6,7])
		logging.info("setting dspu "+dspu+"/GuruVolatileControl#clearFults to "+str(val) )
		japc.setParam(dspu+'/GuruVolatileControl#clearFaults',val)
	if not selectedObjects.dspu:
		return False
	return True

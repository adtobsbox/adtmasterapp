import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
from components.adtintdialog import ADTIntDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.commonfunctions import *
from components.genericPlot import GenericPlot
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
import os
logging.getLogger('matplotlib.font_manager').disabled = True

class PlotDlg(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # Load the dialog's GUI
        DIRNAME = os.path.split(__file__)[0]
        ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/3plots.ui")
        self.buffer1={}
        self.buffer2={}
        self.buffer3={}
        self.buffer4={}
        self.buffer5={}
        self.buffer6={}
        self.buffer7={}
        self.buffer8={}
        self.buffer9={}
        self.buffer10={}
        self.buffer11={}
        self.buffer12={}
        loadUi(ui_file, self)
    def spinboxChanged(self,val):
        if val not in self.buffer1:
            return
        self.plot0.clear()
        self.plot1.clear()
        self.plot2.clear()
        startindex=-1
        stopindex=1
        self.plot0.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer1[val])),self.buffer1[val],'-ob',label='1 Sum Ia')
        self.plot0.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer2[val])),self.buffer2[val],'-or',label='2 Sum Qa')
        self.plot0.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer3[val])),self.buffer3[val],'-oc',label='3 Sum Ib')
        self.plot0.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer4[val])),self.buffer4[val],'-om',label='4 Sum Qb')
        self.plot0.draw()

        self.plot1.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer5[val])),self.buffer5[val],'-ob',label='5 Delta Ia')
        self.plot1.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer6[val])),self.buffer6[val],'-or',label='6 Delta Ib')
        self.plot1.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer9[val])),self.buffer9[val],'-oc',label='9 Delta Ic')
        self.plot1.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer10[val])),self.buffer10[val],'-om',label='10 Delta Id')
        self.plot1.draw()

        self.plot2.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer7[val])),self.buffer7[val],'-oc',label='7 Delta Qa')
        self.plot2.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer8[val])),self.buffer8[val],'-om',label='8 Delta Qb')
        self.plot2.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer11[val])),self.buffer11[val],'-ob',label='11 Delta Qc')
        self.plot2.plot(np.arange(startindex,stopindex,(stopindex-startindex)/len(self.buffer12[val])),self.buffer12[val],'-or',label='12 Delta Qd')
        self.plot2.draw()

    def setSlider(self,start,stop,step):
        self.spinBox.setMinimum(start)
        self.spinBox.setMaximum(stop)
        self.spinBox.setSingleStep(step)

    def run(self):
        self.spinBox.valueChanged.connect(self.spinboxChanged)
        self.show()
        self.spinboxChanged(self.spinBox.value())
        self.exec_()

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    dialog=ADTIntDialog("Start",0)
    start=dialog.get()
    dialog=ADTIntDialog("Stop",1024)
    stop=dialog.get()+1
    dialog=ADTIntDialog("Step",128)
    step=dialog.get()
    if len(range(start,stop,step))>10:
        logging.info("maximum 10 steps is allowed")
        return False
    for beampos in selectedObjects.beampos:
        
        bunch=0
        if "Hor" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
        logging.info("Running test_adt_fine_delay_increments for beampos "+beampos+" and bunch "+str(bunch))
        buffers1={}
        buffers2={}
        buffers3={}
        buffers4={}
        buffers5={}
        buffers6={}
        buffers7={}
        buffers8={}
        buffers9={}
        buffers10={}
        buffers11={}
        buffers12={}
        for i in range(start,stop,step):
            japc.setParam(beampos+'/Clk#delay1',i)
            japc.setParam(beampos+'/Clk#delay2',i)
            japc.setParam(beampos+'/Ttu#trigControl',32)
            time.sleep(1)
            bufsize = 10692
            offset=25
            raw = False
            chanName = 1
            japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
            japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
            time.sleep(2)
            bufid = 256

            buffer1 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer1_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize,'offset':offset})
            buffer2 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer2_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize,'offset':offset})
            buffer3 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer3_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize,'offset':offset})
            buffer4 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer4_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize,'offset':offset})
            buffer5 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer5_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize,'offset':offset})
            buffer6 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer6_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize,'offset':offset})
            buffer7 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer7_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize,'offset':offset})
            buffer8 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer8_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize,'offset':offset})
            buffer9 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer9_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize,'offset':offset})
            buffer10 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer10_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize,'offset':offset})
            buffer11 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer11_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize,'offset':offset})
            buffer12 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize,'offset':offset}))
            buffer12_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize,'offset':offset})

            index = np.where(buffer1_tag != 0)[0][0] 
            xmax=index+15
            xmin=index-15
            if bunch!=None:
                index=index+bunch*3
                xmin=index-15
                xmax=index+15
            
            #can be different for delta
            index = np.where(buffer5_tag != 0)[0][0] 
            xmaxDelta=index+15
            xminDelta=index-15
            if bunch!=None:
                index=index+bunch*3
                xminDelta=index-15
                xmaxDelta=index+15
            

            buffer1=buffer1[xmin:xmax]
            buffers1[i]=buffer1
            
            buffer2=buffer2[xmin:xmax]
            buffers2[i]=buffer2
            
            buffer3=buffer3[xmin:xmax]
            buffers3[i]=buffer3

            buffer4=buffer4[xmin:xmax]
            buffers4[i]=buffer4
            
            buffer5=buffer5[xminDelta:xmaxDelta]
            buffers5[i]=buffer5
            
            buffer6=buffer6[xminDelta:xmaxDelta]
            buffers6[i]=buffer6

            buffer7=buffer7[xminDelta:xmaxDelta]
            buffers7[i]=buffer7
            
            buffer8=buffer8[xminDelta:xmaxDelta]
            buffers8[i]=buffer8
            
            buffer9=buffer9[xminDelta:xmaxDelta]
            buffers9[i]=buffer9
            
            buffer10=buffer10[xminDelta:xmaxDelta]
            buffers10[i]=buffer10
            
            buffer11=buffer11[xminDelta:xmaxDelta]
            buffers11[i]=buffer11
            
            buffer12=buffer12[xminDelta:xmaxDelta]
            buffers12[i]=buffer12


        dialog=PlotDlg()
        dialog.buffer1=buffers1
        dialog.buffer2=buffers2
        dialog.buffer3=buffers3
        dialog.buffer4=buffers4
        dialog.buffer5=buffers5
        dialog.buffer6=buffers6
        dialog.buffer7=buffers7
        dialog.buffer8=buffers8
        dialog.buffer9=buffers9
        dialog.buffer10=buffers10
        dialog.buffer11=buffers11
        dialog.buffer12=buffers12
        dialog.setSlider(start,stop-1,step)
        dialog.plot0.setTitle(beampos + 'raw ADC data Sum signal')
        dialog.plot1.setTitle(beampos + ' raw ADC Delta signal I')
        dialog.plot2.setTitle(beampos + ' raw ADC Delta signal Q')
        dialog.run()
    if not selectedObjects.beampos:
        return False
    return bunches

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running beampos_set_nominal_mode for beampos "+beampos)
        logging.info("setting beampos "+beampos+"/SetNominalSettings" )
        japc.setParam(beampos+'/SetNominalSettings',{})
    if not selectedObjects.beampos:
        return False
    return True

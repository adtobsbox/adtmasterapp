import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        machine="1"
        system="1"
        beam=""
        plane=""
        source="01"
        signal=""
        beamSet=False
        planeSet=False
        signalSet=False
        if "B1" in beampos:
            beam="1"
            beamSet=True
        if "B2" in beampos and beamSet:
            logging.error(" both B1 and B2 occur in the name of "+beampos);
            continue
        if "B2" in beampos:
            beam="2"
            beamSet=True
        if "Hor" in beampos:
            plane="1"
            planeSet=True
        if "Ver" in beampos and planeSet:
            logging.error(" both Ver and Hor occur in the name of "+beampos);
            continue
        if "Ver" in beampos:
            plane="2"
            planeSet=True

        if "Q7" in beampos:
            signal="01"
            signalSet=True
        if "Q8" in beampos and signalSet:
            logging.error(" multiple pickups occur in the name of "+beampos);
            continue
        if "Q8" in beampos:
            signal="02"
            signalSet=True
        if "Q9" in beampos and signalSet:
            logging.error(" multiple pickups occur in the name of "+beampos);
            continue
        if "Q9" in beampos:
            signal="03"
            signalSet=True
        if "Q10" in beampos and signalSet:
            logging.error(" multiple pickups occur in the name of "+beampos);
            continue
        if "Q10" in beampos:
            signal="04"
            signalSet=True

        if not beamSet:
            logging.error("beam not found in "+beampos)
            continue
        if not planeSet:
            logging.error("plane not found in "+beampos)
            continue
        if not signalSet:
            logging.error("pickup not found in "+beampos)
            continue
        sourceID=machine+system+beam+plane+source+signal
        sourceIDInt=int(sourceID,16)
        logging.info("settings source id "+sourceID+" "+str(sourceIDInt)+" for beampos "+beampos )
        japc.setParam(beampos+'/Gtp#sourceDlink1',sourceIDInt)
        japc.setParam(beampos+'/Gtp#sourceDlink2',sourceIDInt)
        japc.setParam(beampos+'/Gtp#sourceDlink3',sourceIDInt)
        japc.setParam(beampos+'/Gtp#sourceDlink4',sourceIDInt)


    return True

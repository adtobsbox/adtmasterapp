from components.logic import SelectedObjects, Variables
from components.filedialog import FileDialog
from components.adtmessagedialog import ADTMessageDialog
from PyQt5.QtWidgets import *
import logging
import components.plotlib
import matplotlib.pyplot as plt
import numpy as np
import time


def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects, variables: Variables):
    filename= variables.get("","obsbox_filename")
    if filename:
        dialog=ADTMessageDialog("Use obsbox file defined in table?")
        if not dialog.get():
            filename=""

    if not filename:
        filename = QFileDialog.getOpenFileName(
            None, 'Open File', plotlib.rootpath, "HDF Files (*.h5)")[0]
    if filename != "":
        file = plotlib.PlotFile(filename)
        if not file.open:
            logging.error("File " + filename + " could not be opened")
            return False

        minBunchLen = 3564
        for element in file.datasets:
            minBunchLen = min(element.cols - 1, minBunchLen)

        text, ok = QInputDialog.getText(None, 'Bunches to plot', 'Select the bunches to plot in the form of "1 2 3 532":')
        if not ok:
            logging.error("plot_obsbox_file cancel by user")
            return False
        else:
            bunchesToPlot=[]
            if text:
                bunchesToPlot=[*map(int ,text.split(' '))]
            if len(bunchesToPlot)==0:
                bunchesToPlot = [x for x in range(minBunchLen)]
                logging.info("could not find any bunches to plot, selecting all")
        # Setup plts
        plt.figure(filename+"_fft")
        plt.clf()
        ax1 = plt.subplot()

        # Plot all datasets
        element=file.datasets[0]
        maxarr = 0
        ylimits = [0, 0]
        ax1.grid(True)
        ax1.set_title("Data captured from "+element.group+" "+element.dataset)
        ax1.set_ylabel("Spectrum amplitude")
        ax1.set_xlabel("Spectrum")
        # For every bunch in the dataset
        ffts=[]
        for i in range(element.cols):
            tempdata = None
            # Check if it only contains zeros
            # if element.data[i][0]==0 and element.data[i][int(len(element.data[i])/2)]==0 and element.data[i][-1]==0:
            #  continue
            # if we dont want to plot this bunch
            if i not in bunchesToPlot:
                continue
            if not np.any(element.data[i]):
                logging.info("skipping bunch "+str(i)+" because its data is all zeros")
                continue
            logging.info("Plotting bunch "+str(i))
            ffts.append(np.abs(np.fft.rfft(element.data[i])))
        fft=np.average(ffts,axis=0)
        fft[0]=0
        ax1.plot(np.array(range(len(fft)))/(len(fft)*2),fft,label="fft")
        ax1.set_xlim([0,0.5])
        plt.show()
        # else:
        #     plt.savefig(file.PathAndFilename)
        # plt.close("all")
        file.close()
        return True

    else:
        logging.info("No file selected, cancelling")
        return False

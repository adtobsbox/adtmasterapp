import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
import numpy as np
import time

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    adcs=["sum_offsetADC0a","sum_offsetADC0b","sum_offsetADC1a","sum_offsetADC1b","delta_offsetADC2a","delta_offsetADC2b","delta_offsetADC3a","delta_offsetADC3b","delta_offsetADC4a","delta_offsetADC4b","delta_offsetADC5a","delta_offsetADC5b"]
    for beampos in selectedObjects.beampos:
        logging.info("Running write_beampos_adc_offset for beampos "+beampos)
        for adc in adcs:
            if variables.get(beampos,adc):
                japc.setParam(beampos+'/BeamPos#'+adc,variables.get(beampos,adc))
            else:
                logging.error("Variable "+adc+" not found for beampos "+beampos)
    if not selectedObjects.beampos:
        return False
    return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
from components.commonfunctions import *

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running beampos_disable_buffer_trigger for beampos "+beampos)
        japc.setParam(beampos+'/BufferSetting#trigSel',0,dataFilterOverride={'channelName': 0})
        japc.setParam(beampos+'/BufferSetting#trigSel',0,dataFilterOverride={'channelName': 1})
        
    if not selectedObjects.beampos:
        return False
    return True

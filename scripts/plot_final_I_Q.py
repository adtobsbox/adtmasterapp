
import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import matplotlib.pyplot as plt
import numpy as np
import time
from components.commonfunctions import *
logging.getLogger('matplotlib.font_manager').disabled = True
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    bunches=getBunchesBeampos(selectedObjects,variables)
    for beampos in selectedObjects.beampos:
        bunch=0
        if "Hor" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B1" in beampos:
            for element in bunches:
                if element[1]=="B1V_bunch":
                    bunch=element[2]
        if "Hor" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2H_bunch":
                    bunch=element[2]
        if "Ver" in beampos and "B2" in beampos:
            for element in bunches:
                if element[1]=="B2V_bunch":
                    bunch=element[2]
        logging.info("Running plot_combined_I_Q_position for beampos "+beampos)
        bufsize = 10692
        offset=25
        raw = False
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256

        buffer13 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer13_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize,'offset':offset})
        buffer14 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+13,'raw': raw, 'size':bufsize,'offset':offset}))
    #    buffer14_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+13,'raw': raw, 'size':bufsize,'offset':offset})
        buffer15 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+14,'raw': raw, 'size':bufsize,'offset':offset}))
    #    buffer15_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+14,'raw': raw, 'size':bufsize,'offset':offset})
        buffer16 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+15,'raw': raw, 'size':bufsize,'offset':offset}))
    #    buffer16_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+15,'raw': raw, 'size':bufsize,'offset':offset})
        buffer17 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize,'offset':offset}))
        buffer17_tag = japc.getParam(beampos+'/BufferAcquisition#flags',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize,'offset':offset})

        index = np.where(buffer13_tag == 3)[0][0]
        xmax=index+15
        xmin=index-15
        
        if bunch!=None:
            index=index+bunch*3
            xmin=index-15
            xmax=index+15
        AngleSum = np.arctan2(buffer14[index],buffer13[index])*180/np.pi
        AngleDelta = np.arctan2(buffer16[index],buffer15[index])*180/np.pi

        plt.figure(beampos+"final_I_Q_position") # modified observation, raw ADC data for Sum, then processed Sum-Delta-Position data
        plt.clf()
        plt.subplot(211)
        plt.plot(buffer13_tag*10000,label='tag Sum final')
        plt.plot(buffer13,'-',label='13 Sum I final')
        plt.plot(buffer14,'-',label='14 Sum Q final')
        plt.plot(buffer15,'-',label='15 Delta I final')
        plt.plot(buffer16,'-',label='16 Delta Q final')
        plt.grid(True)
        plt.legend(loc='upper left')
        plt.xlabel('120 MHz slots')
        plt.xlim(xmin, xmax)
        plt.ylim(np.amin([buffer13[xmin:xmax],buffer14[xmin:xmax],buffer15[xmin:xmax],buffer16[xmin:xmax] ]),np.amax([buffer13[xmin:xmax],buffer14[xmin:xmax],buffer15[xmin:xmax],buffer16[xmin:xmax] ]) )
        #plt.xlim(numpy.where(buffer13_tag == 3)[0][0]-15, numpy.where(buffer13_tag == 3)[0][0]+15)
        plt.title(beampos + ' final Sum/Delta data (1 point per bunch)\nDelta-Sum angle %.1f degrees'%(AngleDelta-AngleSum))

        index = np.where(buffer17_tag == 3)[0][0]
        xmax=index+15
        xmin=index-15
        
        if bunch!=None:
            index=index+bunch*3
            xmin=index-15
            xmax=index+15


        plt.subplot(212)
        plt.plot(buffer17_tag*10000,label='tag position final')
        plt.plot(buffer17,'-',label='17 Bunch position final')
        plt.grid(True)
        plt.legend(loc='upper left')
        plt.xlabel('120 MHz slots')
        plt.xlim(xmin, xmax)
        plt.ylim(np.amin([buffer17[xmin:xmax]]),np.amax([buffer17[xmin:xmax]]) )
        #plt.xlim(numpy.where(buffer17_tag == 3)[0][0]-15, numpy.where(buffer17_tag == 3)[0][0]+15)
        plt.title(beampos + ' final Position data (1 point per bunch)')

        plt.tight_layout()
        plt.show()

    if not selectedObjects.beampos:
        return False
    return bunches

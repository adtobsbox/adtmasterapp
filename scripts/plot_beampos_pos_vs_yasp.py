import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import matplotlib.pyplot as plt
import numpy as np

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	beamposes={}
	#(unit,variable name)
	for element in variables.variables:
		varNameSplit=element[1].split("_")
		if len(varNameSplit)==3 and varNameSplit[0]=="parameters" and varNameSplit[2]=="PositionFinal":
			if (element[0] not in beamposes):
				beamposes[element[0]]=[[float(varNameSplit[1]),variables.variables[element]]]
			else:
				beamposes[element[0]].append([[float(varNameSplit[1]),variables.variables[element]]])
	#beampos=[[yaspPosition0,bpmPosition0],[yaspPosition1,bpmPosition1].....]
	#not necesarily in order
	for element in beamposes:
		beamposes[element]=sorted(beamposes[element], key=lambda x: x[0] )
		yasp=[]
		BPM=[]
		for position in beamposes[element]:
			yasp.append(position[0])
			BPM.append(position[1])
		yasp=np.array(yasp).astype(float)
		BPM=np.array(BPM).astype(float)
		BPM/=32768.0
		plt.figure(element+"yasp_vs_beampos_position")
		plt.clf()
		print(yasp)
		print(BPM)
		plt.plot(yasp,BPM)
		plt.xlim(-1, 1)
		plt.ylim(-1, 1)
		plt.xlabel('YASP position')
		plt.ylabel('BPM position')
		plt.title(element + 'yasp_vs_beampos_position')
		plt.grid(True)
		plt.tight_layout()
		plt.show()
	if beamposes:
		return True
	return False

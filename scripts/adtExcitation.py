import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging
from components.commonfunctions import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
import os
from PyQt5 import QtCore, QtWidgets
from components.excitationLengthProfileCanvas import ExcitationLengthProfileCanvas
from components.windowProfileCanvas import WindowProfileCanvas

class ExcitationDlg(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        # Load the dialog's GUI
        DIRNAME = os.path.split(__file__)[0]
        ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/adtExcitation.ui")
        loadUi(ui_file, self)
        #self.windowProfileCanvas = WindowProfileCanvas(self.windowsProfileWidget, width=5, height=4, dpi=100)
        #self.excitationLengthProfileCanvas = ExcitationLengthProfileCanvas(self.lengthProfileWidget, width=5, height=4, dpi=100)
        self.windowProfileSet.clicked.connect(self.setBunches)
        self.windowProfileClear.clicked.connect(self.clearBunches)
        self.windowProfileClearAll.clicked.connect(self.clearAllBunches)

        self.riseTurnsSpinBox.valueChanged.connect(self.updateExcitationLength)
        self.flatTopTurnsSpinBox.valueChanged.connect(self.updateExcitationLength)
        self.fallTurnsSpinBox.valueChanged.connect(self.updateExcitationLength)
        self.skipTurnsSpinBox.valueChanged.connect(self.updateExcitationLength)
        self.amplitudeSpinBox.valueChanged.connect(self.updateExcitationLength)
        self.show()
        self.updateExcitationLength()
        self.exec_()
    def setBunches(self,checked):
        startBunch=self.startBunchSpinBox.value()
        endBunch=self.endBunchSpinBox.value()
        self.windowsProfileWidget.setBunches(startBunch,endBunch)
    def clearBunches(self,checked):
        startBunch=self.startBunchSpinBox.value()
        endBunch=self.endBunchSpinBox.value()
        self.windowsProfileWidget.clearBunches(startBunch,endBunch)
    def clearAllBunches(self,checked):
        self.windowsProfileWidget.clearAllBunches()
    def updateExcitationLength(self):
        self.lengthProfileWidget.update(self.riseTurnsSpinBox.value(),self.flatTopTurnsSpinBox.value(),self.fallTurnsSpinBox.value(),self.skipTurnsSpinBox.value(),self.amplitudeSpinBox.value())
        pass
def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    dialog=ExcitationDlg()
    return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging
from components.adtintdialog import ADTIntDialog
from components.adtdoubledialog import ADTDoubleDialog
from PyQt5.QtCore import QTimer,QDateTime
from PyQt5.QtWidgets import QDialog
import os
from PyQt5.uic import loadUi
from PyQt5 import QtGui
class ExcitationMeasurement(QDialog):
	def __init__(self,dspus:list,japc: pyjapc.PyJapc,parent=None):
		super().__init__(parent)
		self.japc=japc
		DIRNAME = os.path.split(__file__)[0]
		ui_file = os.path.join(DIRNAME, "..", "ADTMasterApp","ui/dspu_delay_measurement.ui")
		loadUi(ui_file, self)
		self.timer=QTimer()
		self.timer.timeout.connect(self.excite)
		self.time_between_excitation=10000
		self.dspus=dspus

		self.mode=1 # 0=off, 1=pulse,2=cosine,3=sine
		self.N=1 #number of periods per 1 turn excitaiton
		self.frev=11245 #Hz
		self.fS=self.frev*3564*3 # 120MHz clock
		self.FTW=self.N*self.frev*(2**32)/self.fS# frequency tuning word
		
		self.amplitude=1.0*(2**14)
		self.ExcitationA=1
		self.ExcitationB=1
		self.bunches=3563
		self.Nh=3564

	def addLog(self,msg):
		self.text.appendPlainText(msg)
		self.text.moveCursor(QtGui.QTextCursor.End)

	def cancel(self):
		self.close()
	def runExcitation(self):
		self.startButton.setEnabled(False)
		self.excite()
		self.timer.start(self.time_between_excitation)

	def excite(self):
		for dspu in self.dspus:
			self.addLog("Triggering DSPU "+dspu)
			self.japc.setParam(dspu+'/Command',{"TtuTrigManualExcitation":True})
	def run(self):
		dialog=ADTIntDialog("Time in milliseconds between excitations",self.time_between_excitation)
		self.time_between_excitation=dialog.get()
		if self.time_between_excitation<5000 or self.time_between_excitation>3600000:
			logging.error("time must be between 5000 and 3600000 milliseconds")
			return False
		dialog=ADTDoubleDialog("Excitation amplitude",0.5)
		self.amplitude=dialog.get()
		if self.amplitude<0.0 or self.amplitude>1.0:
			logging.error("amplitude must be between 0 and 1")
			return False
		self.amplitude=self.amplitude*(2**14)
		for dspu in self.dspus:
			#so the buffers are not frozen every excitation
			self.japc.setParam(dspu+'/BufferSetting',{"tagEnable":0,"trigSel":0},dataFilterOverride={'channelName':0})

			self.japc.setParam(dspu+'/Excitation',{"Dds1ExcFTW":self.FTW,"Dds1ExcAmplitude":self.amplitude})
			self.japc.setParam(dspu+'/GuruVolatileControl',{"test1":self.mode,"test2":self.bunches})
			self.japc.setParam(dspu+'/GuruControl',{"ttuControl2":self.ExcitationA*1+2**8+self.ExcitationB*2+2**9,})
		self.cancelButton.clicked.connect(self.cancel)
		self.startButton.clicked.connect(self.runExcitation)
		self.show()
		self.exec_()
		return True

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	if not selectedObjects.dspu:
		return False
	excitationMeasurement=ExcitationMeasurement(selectedObjects.dspu,japc)
	return excitationMeasurement.run()

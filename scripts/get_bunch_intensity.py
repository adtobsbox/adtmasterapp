import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    logging.info("Running get_bunch_intensity ")
    intensityB1 = japc.getParam('LHC.BCTFR.A6R4.B1/Acquisition#averageBunchIntensities')
    intensityB2 = japc.getParam('LHC.BCTFR.A6R4.B2/Acquisition#averageBunchIntensities')
    to_return=[]
    to_return.append(("B1","averageBunchIntensities",intensityB1))
    to_return.append(("B2","averageBunchIntensities",intensityB2))
    return to_return

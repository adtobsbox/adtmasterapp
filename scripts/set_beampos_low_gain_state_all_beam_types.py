import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
from components.adtmessagedialog import ADTMessageDialog
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):

    dialog=ADTMessageDialog("This will erase previous configuration, are you sure you want to do this?")
    dialog_val=dialog.get()
    if not dialog_val:
        logging.info("Cancelling set_beampos_in_low_gain_state_all_beam_types from use input")
        return
    val=bitToInt([0,1,2,3,4,7])
    for beampos in selectedObjects.beampos:
        for beam in ["NominalSettings","PilotSettings","HighSettings"]:
            for gain in ["gainDelta","gainSum"]:
                logging.info("Running set_beampos_in_low_gain_state_all_beam_types for beampos "+beampos)
                logging.info("setting beampos "+beampos+"/"+beam+"#"+gain+" to "+str(val) )
                japc.setParam(beampos+'/'+beam+'#'+gain,val)
    if not selectedObjects.beampos:
        return False
    return True

import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    for beampos in selectedObjects.beampos:
        logging.info("Running set_beampos_threshold for beampos "+beampos)
        val=2.5E10
        logging.info("setting beampos "+beampos+"/PilotSettings#maxIntensity to "+str(val) )
        japc.setParam(beampos+'/PilotSettings#maxIntensity',val)
        val=1.6E11
        logging.info("setting beampos "+beampos+"/NominalSettings#maxIntensity to "+str(val) )
        japc.setParam(beampos+'/NominalSettings#maxIntensity',val)
        val=4.0E11
        logging.info("setting beampos "+beampos+"/HighSettings#maxIntensity to "+str(val) )
        japc.setParam(beampos+'/HighSettings#maxIntensity',val)
    if not selectedObjects.beampos:
        return False
    return True

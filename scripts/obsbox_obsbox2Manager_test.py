import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
	good=True
	for manager in ["ObsBox2Manager.LHC.ADT.DEV.B2","ObsBox2Manager.LHC.ADT.DEV.B1","ObsBox2Manager.LHC.ADT.REAL.B1","ObsBox2Manager.LHC.ADT.REAL.B2","ObsBox2Manager.LHC.ADT.BUF.B2","ObsBox2Manager.LHC.ADT.BUF.B1","ObsBox2Manager.LHC.ADT.REAL.mDSPU"]:
		field="alarm_or"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False
		
		field="alarm_temp"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="alarm_vccaux"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="alarm_vccbram"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="alarm_vccint"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="alarm_vccoddr"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="alarm_vccpaux"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="alarm_vccpint"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="align_mismatch"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="busy"
		if(japc.getParam(manager+'/Status#'+field)!=1):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="cpld_firmware"
		if(japc.getParam(manager+'/Status#'+field)!=20201000):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="cpllfbclklost_out"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="cplllock_out"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="databufferreader_reset"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="databufferwriter_reset"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="descriptor_complete"
		if(japc.getParam(manager+'/Status#'+field)!=1):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="descriptor_error"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="descriptor_match_error_counter"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False


		field="descriptor_stopped"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="disperr"
		if(all(japc.getParam(manager+'/Status#'+field))!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="fpga_firmware"
		if(japc.getParam(manager+'/Status#'+field)!=20210600):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="gt_fsm_reset_done"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="gt_mmcm_lock_out"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="gt_reset_done"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="gt_wizard_reset"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="header_found"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="idle_stopped"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="invalid_length"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="magic_stopped"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="moddet"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="notintable"
		if(all(japc.getParam(manager+'/Status#'+field))!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="pipeline_reset"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="read_error"
		if(japc.getParam(manager+'/Status#'+field)!=0):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="rx_los"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="rxbufstat"
		if(all(japc.getParam(manager+'/Status#'+field))!=1):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="rxbyteisaligned"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_alarm_rx_power_high"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_alarm_rx_power_low"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_alarm_temp_high"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_alarm_temp_low"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_alarm_vcc_high"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_alarm_vcc_low"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False
		field="sfp_warning_rx_power_high"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_warning_rx_power_low"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_warning_temp_high"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_warning_temp_low"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_warning_vcc_high"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="sfp_warning_vcc_low"
		if(all(japc.getParam(manager+'/Status#'+field))!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="stream_running"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="three_commas_found"
		if(all(japc.getParam(manager+'/Status#'+field))!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="xadc_reset"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="xdma_busy"
		if(japc.getParam(manager+'/Status#'+field)!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="xdma_descriptor_stop"
		if(japc.getParam(manager+'/Status#'+field)!=False):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False

		field="xdma_run"
		if(japc.getParam(manager+'/Status#'+field)!=True):
			logging.error(manager+'/Status#'+field + " is not correct" )
			good=False
	return good

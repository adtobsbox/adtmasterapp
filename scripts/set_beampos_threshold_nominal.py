import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.commonfunctions import *
import logging
from components.adtintdialog import ADTIntDialog

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):



    for beampos in selectedObjects.beampos:
        old_value=japc.getParam(beampos+'/NominalSettings#threshold')
        dialog=ADTIntDialog("Set threshold for nominal mode for beampos "+beampos,old_value)
        threshold=dialog.get()
        if not threshold:
            logging.info("Operation cancelled by user")
            return False
        logging.info("Running set_beampos_threshold_nominal for beampos "+beampos)

        logging.info("setting beampos "+beampos+"/NominalSettings#threshold to "+str(threshold) )
        japc.setParam(beampos+'/NominalSettings#threshold',threshold)

    if not selectedObjects.beampos:
        return False
    return True

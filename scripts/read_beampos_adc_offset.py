import components.pyjapcinterface as pyjapc
from components.logic import SelectedObjects,Variables
from components.adtmessagedialog import ADTMessageDialog
import logging
import numpy as np
import time

def run(japc: pyjapc.PyJapc, selectedObjects: SelectedObjects,variables : Variables):
    to_return=[]
    beams=[]
    for beampos in selectedObjects.beampos:
        if "B1" in beampos:
            beams.append("B1")
        if "B2" in beampos:
            beams.append("B2")
    if beams:
        if len(beams)==2:
            dialog=ADTMessageDialog("For this both beams must be dumped")
            val=dialog.get()
            if not val:
                logging.info("Cancelling read_beampos_adc_offset from use input")
                return
        else:
            dialog=ADTMessageDialog("For this beam "+beams[0]+" must be dumped")
            val=dialog.get()
            if not val:
                logging.info("Cancelling read_beampos_adc_offset from use input")
                return
        logging.info("User confirmed that beam is dumped")

    for beampos in selectedObjects.beampos:
        logging.info("Running read_beampos_adc_offset for beampos "+beampos)
        chanName = 1
        japc.setParam(beampos+'/BufferSetting#tagEnable',True,dataFilterOverride={'channelName': chanName})
        japc.setParam(beampos+'/BufferSetting#manualFreeze',True,dataFilterOverride={'channelName': chanName})
        time.sleep(2)
        bufid = 256
        bufsize = 16384
        raw = False


        buffer1 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+0,'raw': raw, 'size':bufsize}))
        buffer2 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+1,'raw': raw, 'size':bufsize}))
        buffer3 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+2,'raw': raw, 'size':bufsize}))
        buffer4 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+3,'raw': raw, 'size':bufsize}))
        buffer5 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+4,'raw': raw, 'size':bufsize}))
        buffer6 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+5,'raw': raw, 'size':bufsize}))
        buffer7 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+6,'raw': raw, 'size':bufsize}))
        buffer8 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+7,'raw': raw, 'size':bufsize}))
        buffer9 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+8,'raw': raw, 'size':bufsize}))
        buffer10 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+9,'raw': raw, 'size':bufsize}))
        buffer11 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+10,'raw': raw, 'size':bufsize}))
        buffer12 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+11,'raw': raw, 'size':bufsize}))
        buffer13 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+12,'raw': raw, 'size':bufsize}))
        buffer14 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+13,'raw': raw, 'size':bufsize}))
        buffer15 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+14,'raw': raw, 'size':bufsize}))
        buffer16 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+15,'raw': raw, 'size':bufsize}))
        buffer17 = np.int16(japc.getParam(beampos+'/BufferAcquisition#buffer',dataFilterOverride={'bufferName': bufid+16,'raw': raw, 'size':bufsize}))
        b1mean = np.mean(buffer1)
        b2mean = np.mean(buffer2)
        b3mean = np.mean(buffer3)
        b4mean = np.mean(buffer4)
        b5mean = np.mean(buffer5)
        b6mean = np.mean(buffer6)
        b7mean = np.mean(buffer7)
        b8mean = np.mean(buffer8)
        b9mean = np.mean(buffer9)
        b10mean = np.mean(buffer10)
        b11mean = np.mean(buffer11)
        b12mean = np.mean(buffer12)
        to_return.append((beampos,"sum_offsetADC0a",b1mean))
        to_return.append((beampos,"sum_offsetADC0b",b2mean))
        to_return.append((beampos,"sum_offsetADC1a",b3mean))
        to_return.append((beampos,"sum_offsetADC1b",b4mean))
        to_return.append((beampos,"delta_offsetADC2a",b5mean))
        to_return.append((beampos,"delta_offsetADC2b",b6mean))
        to_return.append((beampos,"delta_offsetADC3a",b7mean))
        to_return.append((beampos,"delta_offsetADC3b",b8mean))
        to_return.append((beampos,"delta_offsetADC4a",b9mean))
        to_return.append((beampos,"delta_offsetADC4b",b10mean))
        to_return.append((beampos,"delta_offsetADC5a",b11mean))
        to_return.append((beampos,"delta_offsetADC5b",b12mean))
    if not selectedObjects.beampos:
        return False
    return to_return
